import React from 'react';
import HomePage from './Pagini/HomePage/HomePage';
import {Routes,Route} from 'react-router-dom'
import Demo from './Demo';
import PaginaQuiz from './PaginaQuiz';
import Logare from './Logare/Logare';
import Inregistrare from './Inregistrare/Inregistrare' 
import ResetareParola from './Pagini/ResetareParola/ResetareParola';
import Navbar from './components/Componente/Navbar/Navbar';
import CardEchipa from './components/Componente/CardEchipa/CardEchipa';
import CapodoperaZilei from './components/Componente/CapodoperaZilei/CapodoperaZilei';
import Profil from './Pagini/Profil/Profil';
import Despre from './Pagini/Despre/Despre';
import PaginaAutor from './Pagini/PaginaAutor/PaginaAutor';
import Artisti from './Pagini/Artisti/Artisti';
import ArtistiAlfabetic from './Pagini/ArtistiAlfabetic/ArtistiAlfabetic';
import Admin from './Pagini/Admin/Admin';
import Artist from './Pagini/Artist/Artist';
import Radio from './components/Componente/Radio/Radio';
import Camera1 from './Pagini/TurVirtual/Camera1';
import Opera from './Pagini/Opera/Opera';
import ArtistiLocali from './Pagini/ArtistiLocali/ArtistiLocali';
import ArtistLocal from './Pagini/ArtistLocal/ArtistLocal';
import Carusel from './components/Componente/OpereAleatoare/Carusel';
import ProfilUtilizator from './Pagini/ProfilUtilizator/ProfilUtilizator';
import ResponsiveAppBar from './components/Componente/Navbar/Navbar2';



function App() {
  
  

  return (
    <>
    <Navbar/>
    
    <Radio/>
    <Routes>
        <Route path="/home" element={<HomePage/>}/>
        <Route path='/' element={<Demo/>}/>
        <Route path='/test' element={<PaginaQuiz/>}/>
        <Route path='/logare' element={<Logare/>}/>
        <Route path='/inregistrare' element={<Inregistrare/>}/>
        <Route path='/resetareParola' element={<ResetareParola/>}/>
        <Route path='/card' element={<CardEchipa/>}/>
        <Route path='/comp' element={<CapodoperaZilei/>} />
        <Route path='/profil' element={<Profil/>} />
        <Route path='/despre' element={<Despre/>} />
        <Route path='/artist' element={<PaginaAutor/>}/>
        <Route path='/artisti' element={<Artisti/>}/>
        <Route path='/turVirtual' element={<Camera1/>} />
        <Route path='/artisti/:litera' element={<ArtistiAlfabetic/>} />
        <Route path='/admin' element={<Admin/>} />
        <Route path='/artist/:numeArtist' element={<Artist/>} />
        <Route path='/opera/:opera' element={<Opera/>} />
        <Route path='/artistiLocali' element={<ArtistiLocali />} />
        <Route path='/artistLocal/:nume' element={<ArtistLocal />} />
        <Route path='/carusel' element={<Carusel /> } />
        <Route path='/profilUtilizator' element={<ProfilUtilizator />} />
      </Routes> 
   
   
    </>
  );
}

export default App;
