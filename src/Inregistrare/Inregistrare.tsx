import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import axios from 'axios'
import Input from "@mui/material/Input"
// import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { Linkuri } from '../Utils/Linkuri';
import { raspuns,Icons,eroareSistem } from '../Utils/Utils';
import { useNavigate } from 'react-router-dom';
import { useCookies } from 'react-cookie';

const theme = createTheme();

export default function Inregistrare() {

    const history=useNavigate()

    interface Erori{
        nume:string,
        prenume:string,
        email:string,
        parola:string,
        numarTelefon:string
    }

    const [nume, setNume] =React.useState<string>("")
    const [prenume, setPrenume]=React.useState<string>("")
    const [email,setEmail]=React.useState<string>("")
    const [numarTelefon,setNumarTelefon]=React.useState<string>("")
    const [parola,setParola]=React.useState<string>("")
    const campObligatoriu:string="Acest camp este obligatoriu"
    const eroare={nume:"",prenume:"",email:"",parola:""}
    const [imagine, setImagine]=React.useState<Blob>()
    const [erori, setErori]=React.useState<Erori>()
    const [cookies,setCookies]=useCookies(['jwt'])

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        console.log({
            email: data.get('nume'),
            password: data.get('parola'),
            imagine:data.get('btn-upload')
        });

        const regex:RegExp = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        const regexTelefon:RegExp=/^(?=0[723][2-8]\d{7})(?!.*(.)\1{2,}).{10}$/i;
        let errors:Erori={
            nume:"",
            prenume:"",
            parola:"",
            email:"",
            numarTelefon:""
        }
    
       let valid:boolean=true
       if(!nume){
           valid=false
           errors.nume=campObligatoriu
       }
       if(!prenume){
           valid=false
           errors.prenume=campObligatoriu
       }

        if(!email){
            valid=false
            errors.email=campObligatoriu
        }else if(!regex.test(email)){
            errors.email="Introduceti o adresa de mail valida"
        }
        
        if(!numarTelefon){
            valid=false
            errors.numarTelefon=campObligatoriu
        }else if(numarTelefon.length>10){
            valid=false
            errors.numarTelefon="Numarul de telefon trebuie sa fie format din 10 cifre"
        }else if(!regexTelefon.test(numarTelefon)){
            valid=false
            errors.numarTelefon="Introduceti un numar de telefon valid"
        }
        if(!parola){
            valid=false
            errors.parola=campObligatoriu
        }else if(parola.length<4){
            valid=false
            errors.parola="Parola trebuie sa aiba cel putin 4 caractere"
        }
        
        setErori(errors)
        if(valid){
            console.log(valid)
            const formData=new FormData()
          
            formData.append("nume",nume)
            formData.append("prenume",prenume)
            formData.append("numarTelefon",numarTelefon)
            formData.append("email",email)
            formData.append("parola",parola)
            formData.append("imagine",imagine!)
            const res= await fetch(Linkuri.backend+Linkuri.utilizator+Linkuri.inregistrare,{
                body:formData,
                method:"post",
                
            })

            const data=await res.json()

            switch(data){
                case "NUME":
                    raspuns('Atentie',Icons.warning,'Exista un utilizator cu acest nume')
                    break;
                case "EMAIL":
                    raspuns("ATENTIE",Icons.warning,"Exista deja un utilizator cu acest mail")
                    break;
                case "TELEFON":
                    raspuns("ATENTIE",Icons.warning,"Exista deja un utilizator cu acest numar de telefon");
                    break;
                case "SUCCESS":
                    raspuns("FELICITARI",Icons.succes,"Contul dumneavoastra a fost inregistrat cu succes");
                    setCookies('jwt',res.headers.get('Authorization'),{path:'/'})
                    history("/")
                    break;
                
                default:
                    eroareSistem();
                    break;
            }

            // .then((res:any)=>{
            //     console.log(res.json())
            //      res.json() 
            // }).then(data=>console.log("DATA:\n"+data)).catch((err)=>console.log(err))
            // console.log(formData)
            
        }
       
    };

    

    const selectareImagine=(event:any)=>{
        console.log(event.target.files[0])
        setImagine(event.target.files[0])
    }

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                        {/* <LockOutlinedIcon /> */}
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Inregistrare
                    </Typography>
                    <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    autoComplete="given-name"
                                    name="nume"
                                    required
                                    fullWidth
                                    id="nume"
                                    label="Nume"
                                    value={nume}
                                    onChange={(ev)=>setNume(ev.target.value)}
                                    autoFocus
                                    error={erori?.nume.length!==0}
                                    helperText={erori?.nume}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    required
                                    fullWidth
                                    id="prenume"
                                    label="Prenume"
                                    name="prenume"
                                    autoComplete="family-name"
                                    value={prenume}
                                    onChange={(ev)=>setPrenume(ev.target.value)}
                                    error={erori?.prenume.length!==0}
                                    helperText={erori?.prenume}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email"
                                    name="email"
                                    type="email"
                                    autoComplete="email"
                                    value={email}
                                    onChange={(ev)=>setEmail(ev.target.value)}
                                    error={erori?.email.length!==0}
                                    helperText={erori?.email}
                                />
                            </Grid>

                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id='telefon'
                                    label='Numar Telefon'
                                    name='telefon'
                                    value={numarTelefon}
                                    onChange={(ev)=>setNumarTelefon(ev.target.value)}
                                    type='text'
                                    error={erori?.numarTelefon.length!==0}
                                    helperText={erori?.numarTelefon}
                                    />
                            </Grid>
                            
                            <Grid item xs={12}>
                                <label htmlFor="btn-upload">
                                <input
                                        id="btn-upload"
                                        name="btn-upload"
                                        style={{ display: 'none' }}
                                        type="file"
                                        accept="image/*"
                                        onChange={(ev)=>selectareImagine(ev)}
                                         />
                                    <Button
                                        className="btn-choose"
                                        variant="outlined"
                                        fullWidth
                                        component="span" >
                                        Selectare Imagine Profil
                                    </Button>
                                </label>
                               
                            </Grid>
                            
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name="parola"
                                    label="Parola"
                                    type="password"
                                    id="parola"
                                    autoComplete="new-password"
                                    value={parola}
                                    onChange={(ev)=>setParola(ev.target.value)}
                                    error={erori?.parola.length!==0}
                                    helperText={erori?.parola}
                                />
                            </Grid>
                            
                        </Grid>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                        >
                            Sign Up
                        </Button>
                        <Grid container justifyContent="flex-end">
                            <Grid item>
                                <Link href="/logare" variant="body2">
                                   Aveti cont? Inregistrati-va!
                                </Link>
                            </Grid>
                        </Grid>
                    </Box>
                </Box>

            </Container>
        </ThemeProvider>
    );
}