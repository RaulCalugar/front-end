import React from "react";

import { amestecaRaspunsuri } from "./Utils";

export enum Dificultate{
    USOR='easy',
    MEDIU='medium',
    GREU='hard'
}

export enum CATEGORIE{
    ARTA="25",
    ISTORIE="23"
}


export type Intrebare={
    category: string;
    correct_answer: string;
    difficulty: string;
    incorrect_answers: string[];
    question: string;
    type: string;
}

export type Raspunsuri= Intrebare & {raspunsuri:string[]};

export const obtineIntrebari=async(
    numarIntrebari:number,
    dificultate:Dificultate,
    categorie:CATEGORIE): Promise<Raspunsuri[]>=>{
    // const url= `https://opentdb.com/api.php?amount=${numarIntrebari}&category=${categorie}&difficulty=${dificultate}&type=multiple`
    const url= `https://opentdb.com/api.php?amount=${numarIntrebari}&category=${categorie}&difficulty=${dificultate}&type=multiple`
    const data = await (await fetch(url)).json();
    return data.results.map((question: Intrebare) => ({
        ...question,
        raspunsuri: amestecaRaspunsuri([...question.incorrect_answers, question.correct_answer])
      }))


    // return data.results.map((intrebare:Intrebare)=>(
    //     {
    //         ...intrebare,
    //         raspunsuri:amestecaRaspunsuri([...intrebare.raspunsuriGresite,intrebare.raspunsCorect]),
    //     }
    // ))
}

export const fetchQuizQuestions = async (amount: number, difficulty: Dificultate): Promise<Raspunsuri[]> => {
    const endpoint = `https://opentdb.com/api.php?amount=${amount}&difficulty=${difficulty}&type=multiple`;
    const data = await (await fetch(endpoint)).json();
    return data.results.map((question: Intrebare) => ({
        ...question,
        raspunsuri: amestecaRaspunsuri([...question.incorrect_answers, question.correct_answer])
      }))
  };


  