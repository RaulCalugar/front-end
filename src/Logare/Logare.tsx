import React, { useState, useEffect } from "react";
import { useCookies } from 'react-cookie'
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
// import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { Linkuri, operaLinks } from "../Utils/Linkuri";
import { Raspunsuri, raspuns, Icons, eroareSistem } from "../Utils/Utils";
import { useNavigate } from "react-router-dom";
import { bake_cookie, read_cookie, delete_cookie } from 'sfcookies';
const theme = createTheme();

export default function Logare() {

  const [email, setEmail] = useState<string>("")
  const [parola, setParola] = useState<string>("")
  const [link, setLink] = useState<string>("")
  const [cookie,setCookie]=useCookies(['jwt'])
  const history=useNavigate()
  useEffect(() => {

    fetch("http://localhost:8080/opera/pozaOpera")
      .then(function (body) {
        return body.text(); 
      }).then(function (data) {
        console.log(data);
        setLink(data)
      }).catch(()=>eroareSistem());
  }, [])

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {

    event.preventDefault()

    const dto = {
      "email": email,
      "parola": parola
    }

    const res = await fetch(Linkuri.backend + Linkuri.utilizator + Linkuri.logare, {
      body: JSON.stringify(dto),
      method: "post",
      headers: {
        "Access-Control-Allow-Origin": "*",
        'Content-Type': 'application/json',
      }
    })


    const data = await res.json()
    switch (data) {
      case Raspunsuri.DATE_GRESITE:
        raspuns('EROARE', Icons.eroare, "A-ti introdus un email sau o parola gresita");
        break;
      case Raspunsuri.SUCCESS:
        raspuns("FELICITARI", Icons.succes, "V-ati logat cu succes");
        setCookie('jwt',res.headers.get('Authorization'),{path:'/'})
        history("/");
        break;
      default:
        eroareSistem();
        break;

    }

  };

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: '100vh' }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage: `url(${link})`, //'url(https://source.unsplash.com/random)',
            backgroundRepeat: 'no-repeat',
            backgroundColor: (t) =>
              t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
            backgroundSize: 'cover',
            backgroundPosition: 'center',
          }}
        />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Box
            sx={{
              my: 8,
              mx: 4,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
              {/* <LockOutlinedIcon /> */}
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign in
            </Typography>
            <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 1 }}>
              <TextField
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                value={email}
                onChange={(ev) => setEmail(ev.target.value)}
                autoFocus
              />
              <TextField
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={parola}
                onChange={(ev) => setParola(ev.target.value)}
              />

              
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Sign In
              </Button>
              <Grid container>
                <Grid item xs>
                  <Link href="/resetareParola" variant="body2">
                    A-ti uitat parola?
                  </Link>
                </Grid>
                <Grid item>
                  <Link href="/inregistrare" variant="body2">
                    {"Nu aveti un cont? Inregistrati-va!"}
                  </Link>
                </Grid>
              </Grid>

            </Box>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}