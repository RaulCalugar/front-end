import React, { useState,useEffect } from "react";
import CardIntrebare from "./components/CardIntrebare";
import {fetchQuizQuestions, CATEGORIE, Dificultate, obtineIntrebari } from "./Api";
import Navbar from "./components/Componente/Navbar/Navbar";
import { Linkuri } from "./Utils/Linkuri";
import { IIntrebare, IRaspuns,Demo,Raspunsuri,raspuns,Icons,eroareSistem } from "./Utils/Utils";
import { amestecaRaspunsuri } from "./Utils";
import { useCookies } from "react-cookie";
import { bake_cookie, read_cookie, delete_cookie } from 'sfcookies';
import './PaginaQuiz.css'

import { stilGlobal } from "./PaginaQuiz.styles";
const TOTAL_INTREBARI = 10

export type Raspuns = {
    intrebare: string;
    raspuns: string;
    correct: boolean;
    raspunsCorect: string;
}


const PaginaQuiz:React.FC=()=> {

   
    const [incarca, setIncarca] = useState(false);
    const [intrebari, setIntrebari] = useState<Raspunsuri[]>([]);
    const [numarIntrebare, setNumarIntrebare] = useState(0);//Numarul intrebarii
    const [raspunsuri, setRaspunsuri] = useState<Raspuns[]>([]);// Raspunsurile utilizatorului
    const [scor, setScor] = useState(0)
    const [oraInceput,setOraInceput]=useState<string>("")
    const [oraSfarsit,setOraSfarsit]=useState<string>("")
    const [sfarsit, setSfarsit] = useState(true)
    const [questions, setQuestions] = useState<Raspunsuri[]>([]);
    const [cookies, setCookies] = useCookies(["jwt"]);
    const [listaIntrebari,setListaIntrebari]=useState<Array<IIntrebare>>([])
    const [raspunsuriUtilizator,setRaspunsuriUtilizator]=useState<IRaspuns[]>([])
    const [numarIntrebari, setNumarIntrebari] = useState<number>(0)
    const [demo,setDemo]=useState<Array<Demo>>([])

    
    
    useEffect(()=>{
        const preluareIntrebari=async()=>{
            const data=await fetch(Linkuri.backend+Linkuri.intrebare+Linkuri.intrebariTest,{
                headers: {
                    'Authorization': cookies.jwt
                  }
            });
            const json=await data.json()
            setListaIntrebari(json)
            console.log(json)
            setNumarIntrebari(json.length)
        }
        preluareIntrebari()
        
    },[])

    const startQuiz =  () => {
        let temp:Demo[]=[]
            for(var i=0;i<listaIntrebari.length;i++){
                let rasp:string[]=[listaIntrebari[i].raspunsCorect,listaIntrebari[i].raspunsGresit1,listaIntrebari[i].raspunsGresit2,listaIntrebari[i].raspunsGresit3]
                rasp=amestecaRaspunsuri(rasp)
                let aux:Demo={
                    enunt:listaIntrebari[i].enunt,
                    raspunsCorect:listaIntrebari[i].raspunsCorect,
                    raspunsGresit1:listaIntrebari[i].raspunsGresit1,
                    raspunsGresit2:listaIntrebari[i].raspunsGresit2,
                    raspunsGresit3:listaIntrebari[i].raspunsGresit3,
                    imagine:listaIntrebari[i].imagine,
                    raspunsuri:rasp
                }
                temp.push(aux)
                

            }
        console.log(temp)
        setDemo(temp)
        const data:Date=new Date();
        setOraInceput(formatareData(data))
        
        setIncarca(true);
        setSfarsit(false);

        setScor(0)
        setRaspunsuri([])
        setRaspunsuriUtilizator([])
        setNumarIntrebare(0)
        setIncarca(false)
    }

    const verificaRaspuns = (e: React.MouseEvent<HTMLButtonElement>) => {

        if(!sfarsit){
            // Raspunsuri
            const raspuns=e.currentTarget.value

            // Vf daca e raspuns bun
            const vf=listaIntrebari[numarIntrebare].raspunsCorect === raspuns;
            
            if(vf)setScor(prev=>prev+1)
            // Adun la score daca e raspuns bun

            // !!! PROGRESS

            const rez:IRaspuns={
                intrebare:listaIntrebari[numarIntrebare],
                raspunsUtilizator:raspuns,
                punctaj:vf ? 1 : 0
            }

            const raspunss:Raspuns={
                intrebare:listaIntrebari[numarIntrebare].enunt,
                raspuns,
                correct:vf,
                raspunsCorect:listaIntrebari[numarIntrebare].raspunsCorect
            }
            setRaspunsuri(prev=>[...prev,raspunss])
            setRaspunsuriUtilizator(prev=>[...prev,rez])
        }
    }

    const urmatoareaIntrebare = () => {
        // Mers la urmatoarea intrebare, daca nu este ultima

        const intrebareaUrmatoare=numarIntrebare+1;
        if(intrebareaUrmatoare===TOTAL_INTREBARI){
            setSfarsit(true)
        }else{
            setNumarIntrebare(intrebareaUrmatoare)
        }
    }

    function padTo2Digits(num: number) {
        return num.toString().padStart(2, '0');
      }

    function formatareData(date:Date){
        return (
            [
              padTo2Digits(date.getDate()),
              padTo2Digits(date.getMonth() + 1),
              date.getFullYear(),
            ].join('-') +
            ' ' +
            [
              padTo2Digits(date.getHours()),
              padTo2Digits(date.getMinutes()),
              padTo2Digits(date.getSeconds()),
            ].join(':')
          );
    }

    const inregistrareProgres=async(e:React.ChangeEvent<HTMLInputElement>)=>{
        e.preventDefault()
        const data:Date=new Date();
        setOraSfarsit(formatareData(data))
        const dto={
            oraInceput,
            oraSfarsit,
            'raspunsuri':raspunsuriUtilizator,
            'token':cookies.jwt,
            scor
        }

        const res = await fetch(Linkuri.backend + Linkuri.utilizator + Linkuri.inregistrareProgres, {
            body: JSON.stringify(dto),
            method: "post",
            headers: {
              'Content-Type': 'application/json',
              'Authorization': cookies.jwt
            }
          })

          const temp=await res.json()
          if(Raspunsuri.SUCCESS===temp){
            raspuns('Felicitari',Icons.succes,'Testul tocmai s-a incheiat')
          }


        startQuiz()
    }

    

    return (
        <>
            
            <h1 className="titlu">Quiz</h1>
            
            {sfarsit || raspunsuri.length === TOTAL_INTREBARI ?
                (<button className="start" onClick={() => startQuiz()}>
                    Incepe Testul
                </button>) : null
            }

            

            {raspunsuri.length === TOTAL_INTREBARI ?(
                <button onClick={(e:any)=>inregistrareProgres(e)}>Incepe alt test</button>) :(<h1>Start</h1>)
            }

            {!sfarsit ? (<p className="scor">Scor: {scor}</p>) : null}
            
            {incarca &&  <p className="incarcare">Incarcare Intrebari</p>}
           

            {!incarca && !sfarsit && ( <CardIntrebare
                numarIntrebare={numarIntrebare+1}
                totalIntrebari={TOTAL_INTREBARI}
                intrebare={demo[numarIntrebare].enunt}
                raspunsuri={demo[numarIntrebare].raspunsuri}
                raspunsCorect={demo[numarIntrebare].raspunsCorect}
                raspunsUtilizator={raspunsuri ? raspunsuri[numarIntrebare]:undefined}
                callback={verificaRaspuns}
            />)} 
         
            {!sfarsit && !incarca && raspunsuriUtilizator.length===numarIntrebare+1 && numarIntrebare!==TOTAL_INTREBARI-1 ? (
                <button className="urmator" onClick={() => urmatoareaIntrebare()}>Urmatoarea Intrebare</button>
            ):null}

            
            
        </>
    )
}

export default PaginaQuiz