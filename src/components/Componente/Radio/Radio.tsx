import React,{useState,useEffect} from 'react'
import { RadioBrowserApi } from "radio-browser-api";
import { Station } from 'radio-browser-api';
import AudioPlayer from "react-h5-audio-player";
import {Box} from "@mui/material"
import "react-h5-audio-player/lib/styles.css";
import CSS from 'csstype';
import "./Radio.css"

export default function Radio(){

    const [statie,setStatie]=useState<Station>()
    const preluareStatie=async()=>{

        const api = new RadioBrowserApi('Radio')

        const stations = await api
        .searchStations({
            language: "english",
            tag: "classical",
            limit: 30,
        })
        .then((data) => {
            return data;
        });
        setStatie(stations[1])

        return stations
    }

    useEffect(()=>{
        preluareStatie().then((data)=>{
            setStatie(data[1])
        })
    },[])

    return(
        <Box>
            
            <Box sx={{...stilStatie}}>
                <AudioPlayer
                    className="player"
                    src={statie?.urlResolved}
                    showJumpControls={false}
                    showSkipControls={false}
                    layout="stacked"
                    customProgressBarSection={[]}
                    autoPlayAfterSrcChange={true}
                    />
            </Box>
        </Box>
    )

}

const stilStatie:CSS.Properties={
    
    border:"1px solid rgb(76, 62, 95)",
    margin: '0.25em',
    borderRadius: '10px',
    padding: '1em',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width:'300px'
}