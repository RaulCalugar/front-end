import React, { useState,useEffect } from "react";
import {
  AppBar,
  Button,
  InputBase,
  Toolbar,
  CssBaseline,
  Typography,
} from "@material-ui/core";
import { Avatar,Drawer, Divider, IconButton, List, ListItem, ListItemButton, ListItemIcon, ListItemText, Menu, MenuItem } from "@mui/material";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { Link, useNavigate } from "react-router-dom";
import { Linkuri } from "../../../Utils/Linkuri";
import ModalAdaugareReview from "../Modale/ModalAdaugareReview/ModalAdaugareReview";
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import { useCookies } from "react-cookie";
import { styled, useTheme } from '@mui/material/styles';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import PaletteIcon from '@mui/icons-material/Palette';
import BrushIcon from '@mui/icons-material/Brush';
import LanguageIcon from '@mui/icons-material/Language';
import PublicIcon from '@mui/icons-material/Public';
import ReviewsIcon from '@mui/icons-material/Reviews';
import QuizIcon from '@mui/icons-material/Quiz';
import { getImageFromByteArray } from "../../../Utils/Utils";

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    backgroundColor: 'rgb(111, 15, 23)',
  },
  navlinks: {
    marginLeft: theme.spacing(10),
    display: "flex"
  },
  logo: {
    cursor: "pointer",
  },
  link: {
    textDecoration: "none",
    color: "white",
    fontSize: "15px",
    marginLeft: theme.spacing(20),
    "&:hover": {
      color: "yellow",
      borderBottom: "1px solid white",
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: theme.palette.common.white,
    '&:hover': {
      backgroundColor: theme.palette.common.white,
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
}));



function Navbar() {
  const classes = useStyles();

  const history = useNavigate();

  const [addReview, setAddReview] = useState<boolean>(false)
  const [cookies, setCookies] = useCookies(['jwt'])
  const [sideBar, setSideBar] = useState<boolean>(false)
  const [pozaProfil,setPozaProfil]=useState<string>("")
  const [meniu,setMeniu]=useState<boolean>(false)

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };


  const theme = useTheme();

  const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  }));

  useEffect(()=>{
    const pozaProfil=async()=>{
      const data=await fetch(Linkuri.backend+Linkuri.utilizator+Linkuri.pozaProfil,{
        headers: {
          'Authorization': cookies.jwt
        }
      })
      const json=await data.text()
      setPozaProfil(json)
      // setPozaProfil(getImageFromByteArray(json))
    }

    if(cookies.jwt!=""){
      pozaProfil()
    }
  },[cookies.jwt])

  return (
    <>
      <AppBar position="static" className={classes.root}>
        <CssBaseline />

        <Toolbar>
          <Typography variant="h4" className={classes.logo} onClick={() => setSideBar(true)}>
            Muzart
          </Typography>
          <div className={classes.navlinks}>
            <Button onClick={() => history("/")} className={classes.link}>
                    Acasa
            </Button>
            <Button onClick={() => history("/despre")} className={classes.link}>
                    Despre
            </Button>
            <Button onClick={() => history("/artistiLocali")} className={classes.link}>
               Artisti Locali
           </Button>
            {
              cookies.jwt === "" ? (
                <>
                  <Button onClick={() => history("/logare")} className={classes.link}>
                    Logare
                  </Button>
                  <Button onClick={() => history("/inregistrare")} className={classes.link}>
                    Inregistrare
                  </Button>
                
                  {/* <Link to="/logare" className={classes.link}>
                    Logare
                  </Link>
                  <Link to="/inregistrare" className={classes.link}>
                    Inregistrare
                  </Link> */}
                </>
              ) : (
                <>
                  <Button onClick={() => setAddReview(true)} className={classes.link}>
                    Adauga Review
                  </Button>
                  <Button onClick={() => history("/test")} className={classes.link}>
                    Start Quiz
                  </Button>
                  <Button onClick={() => setCookies('jwt', "", { path: '/' })} className={classes.link}>
                    LogOut
                  </Button>   
                  <Avatar onClick={(e:any)=>handleClick(e)} sx={{marginLeft:'100px', width: 56, height: 56}} src={`data:image/jpeg;base64,${pozaProfil}`} />
                  
                  <Menu 
                    anchorEl={anchorEl}
                    open={open}
                    onClose={handleClose}
                    MenuListProps={{
                      'aria-labelledby': 'basic-button',
                    }}
                  >
                      <MenuItem onClick={()=>history('/profilUtilizator')}>Profil</MenuItem>
                      <MenuItem>Log Out</MenuItem>
                  </Menu>

                </>
              )
            }
           

          </div>
        </Toolbar>

        {/* Componenta de drawer din stanga paginii */}
        <Drawer sx={{
          width: '240px',
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            width: '240px',
            boxSizing: 'border-box',
          },
        }}
          variant="persistent"
          anchor="left"
          open={sideBar}>

          <DrawerHeader>
            <IconButton onClick={() => setSideBar(false)}>
              {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
            </IconButton>
          </DrawerHeader>
          <List>
            <ListItem >
              <ListItemButton onClick={() => history("/artisti")}>
                <ListItemIcon>
                  <BrushIcon />
                </ListItemIcon>
                <ListItemText primary={"Artisti"} />
              </ListItemButton>
            </ListItem>
            <ListItem>
              <ListItemButton onClick={() => history("/artistiLocali")}>
                <ListItemIcon>
                  <PaletteIcon />
                </ListItemIcon>
                <ListItemText primary={"Artisti Locali"} />
              </ListItemButton>
            </ListItem>
            <ListItem>
              <ListItemButton>
                <ListItemIcon>
                  <LanguageIcon />
                </ListItemIcon>
                <ListItemText primary={"Nationalitati"} />
              </ListItemButton>
            </ListItem>
            <ListItem>
              <ListItemButton>
                <ListItemIcon>
                  <PublicIcon />
                </ListItemIcon>
                <ListItemText primary={"Curente Artistice"} />
              </ListItemButton>
            </ListItem>

          </List>
          <Divider />
          {
            cookies.jwt != "" ? (
              <>
                <ListItem>
                  <ListItemButton onClick={() => setAddReview(true)}>
                    <ListItemIcon>
                      <ReviewsIcon />
                    </ListItemIcon>
                    <ListItemText primary={"Adaugare Review"} />
                  </ListItemButton>
                </ListItem>
                <ListItem>
                  <ListItemButton onClick={() => history('/test')}>
                    <ListItemIcon>
                      <QuizIcon />
                    </ListItemIcon>
                    <ListItemText primary={"Test cultura generala"} />
                  </ListItemButton>
                </ListItem>
              </>
            ) : (null)
          }

        </Drawer>
      </AppBar>
     


      <ModalAdaugareReview open={addReview} setOpen={setAddReview} />
    </>
  );
}
export default Navbar;