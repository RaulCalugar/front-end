import React from "react";
import "./EchipaNoastra.css"
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import CardEchipa from "../CardEchipa/CardEchipa";


export default function EchipaNoastra() {

    return (

        <>
            <div style={{ display: "flex" }}>



                <div className="container">
                    <div className="containerPoze">
                        <div className="titluri">
                            <span className="t1">
                                Descopera Echipa Noastra
                            </span>
                            <h1 className="t2">
                                Iata Echipa Noastra Fabuloasa
                            </h1>
                        </div>

                        <div className="slider">
                            <div className="slider2">
                                <CardEchipa />
                                <CardEchipa />
                                <CardEchipa />
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </>
    )
}


/*

  <Card sx={{ maxWidth: 345 ,marginLeft:10,marginRight:10}}>
                                <CardMedia
                                    component="img"
                                    alt="green iguana"
                                    height="140"
                                    image="https://uploads6.wikiart.org/images/nicolae-grigorescu/self-portrait.jpg!Portrait.jpg"
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="div">
                                        Lizard
                                    </Typography>
                                    <Typography variant="body2" color="text.secondary">
                                        Lizards are a widespread group of squamate reptiles, with over 6,000
                                        species, ranging across all continents except Antarctica
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Button size="small">Share</Button>
                                    <Button size="small">Learn More</Button>
                                </CardActions>
                            </Card>
                            <Card sx={{ maxWidth: 345 }}>
                                <CardMedia
                                    component="img"
                                    alt="green iguana"
                                    height="140"
                                    image="https://uploads6.wikiart.org/images/nicolae-grigorescu/self-portrait.jpg!Portrait.jpg"
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="div">
                                        Lizard
                                    </Typography>
                                    <Typography variant="body2" color="text.secondary">
                                        Lizards are a widespread group of squamate reptiles, with over 6,000
                                        species, ranging across all continents except Antarctica
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Button size="small">Share</Button>
                                    <Button size="small">Learn More</Button>
                                </CardActions>
                            </Card>

                            */