import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { Button } from "@mui/material"

interface ExpandMoreProps extends IconButtonProps {
    expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
})(({ theme, expand }) => ({
    transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
    }),
}));

export default function CardEchipa() {
    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    return (
        <div style={{ marginTop: "40px", marginLeft: "30%" }}>
            <Card sx={{ maxWidth: 300 }}>

                <CardMedia
                    component="img"
                    height="194"
                    image="https://source.unsplash.com/random"
                    alt="Paella dish"
                />
                <CardContent>
                    <Typography color="red" sx={{
                        "fontSize": 20
                    }}>
                        Emanuel Iorga
                    </Typography>
                    <Typography color='orange'>
                        Ghid Turistic
                    </Typography>
                    <Typography>
                        Emanuel este un tanar ambitios de 24 de ani. Este ghid in cadrul muzeului, avand cunostinte vaste din domenii precum istorie, geografie si arta. Este o placeere sa fie ghid.
                    </Typography>
                </CardContent>
                <CardActions disableSpacing sx={{justifyContent:"center"}}>
                <Button variant="contained" sx={{justifyContent:"center"}}>Detalii</Button>

                </CardActions>

            </Card>
        </div>
    );
}