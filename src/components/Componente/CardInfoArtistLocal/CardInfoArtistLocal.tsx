import React,{FC} from 'react'
import { IArtistLocal,getImageFromByteArray } from '../../../Utils/Utils'
import {
    Avatar,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Checkbox,
    IconButton,
    Typography,
    CardActionArea,
    Button,
    Box
} from "@mui/material";

interface IData {
    info: IArtistLocal
}

const CardInfoArtistLocal: FC<IArtistLocal> = ({ descriere,email,judet,localitate,nume,prenume,premii,studii,pozaProfil,opere,telefon }) =>{

    return(
        <>
        <Box flex={4} p={{ xs: 0, md: 2 }}>
        <Card sx={{ margin: 5 }}>
            <div style={{ display: "flex",width:"40%", height: "70%" }}>
                <CardMedia
                    component="img"
                    height="20%"
                    width="30% !important"
                    
                    image={"data:image/png;base64," + pozaProfil}
                    alt={nume+" "+prenume}

                />
                {/* <img src={"data:image/png;base64,["+pozaProfil+"]"} alt="" /> */}
                <CardContent>
                    <Typography variant="body2" color="text.primary">
                        Nume Artist:{nume+" "+prenume}
                    </Typography>
                    
                    <Typography variant="body2" color="text.secondary">
                        Studii:{studii}
                    </Typography>

                    <Typography variant="body2" color="text.secondary">
                        {premii}
                    </Typography>
                </CardContent>
            </div>
           
        </Card>
    </Box>
    </>
    )
}

export default CardInfoArtistLocal;

