import React, { FC, useState, useEffect } from "react";
import { Linkuri } from "../../../Utils/Linkuri";
import { eroareSistem, Raspunsuri, IOpera, IArtist,raspuns,Icons } from "../../../Utils/Utils";
import { Box, Typography } from "@mui/material"
import {Button,Card,CardMedia,CardContent,CardActions} from "@mui/material"
import CardInfoAutor from "../../../components/Componente/CardInfoAutor/CardInfoAutor";
import SimpleReactLightbox, { SRLWrapper } from 'simple-react-lightbox';
import { useNavigate } from "react-router-dom";
import Swal from 'sweetalert2'
import CSS from 'csstype';
import { useCookies } from "react-cookie";
import EditareArtist from "../EditareArtist/EditareArtist";

interface INumeAutor {
    numeAutor: string,
    load:boolean,
    setLoad:(open:boolean)=>void;
}

const InfoAutor: FC<INumeAutor> = ({ numeAutor,load,setLoad }) => {

    const [urls, setUrls] = useState<Array<string>>([]);
    const [nume, setNume] = useState<Array<string>>([]);
    const [opere, setOpere] = useState<Array<IOpera>>([])
    const [artist, setArtist] = useState<IArtist>()
    const [toggle,setToogle]=useState<boolean>(false)
    const [cookie,setCookie]=useCookies(['jwt'])

    const history=useNavigate()

    const stergeOpera=async(id:number)=>{
        Swal.fire({
            title:'Sigur doriti sa stergeti aceasta opera de arta',
            icon:'warning',
            showCancelButton:true,
            cancelButtonText:'Anulare',
            confirmButtonText:'Stergere'
        }).then((res)=>{
            if(res.isConfirmed){
                // !!
                
                fetch(Linkuri.backend+Linkuri.opera+Linkuri.stergereOpera+id, { method: 'DELETE',headers: {
                    'Authorization': cookie.jwt
                  } })
                .then(async response => {
                    const data = await response.json();
                    if(Raspunsuri.SUCCESS===data){
                        raspuns("SUCCES",Icons.succes,"Opera Stearsa Cu Succes");
                        setToogle(!toggle)
                    }
                   
                })
                
                
                
            }
        })
    }

    useEffect(() => {
        setLoad(true)
        const obtinePicturi = async () => {
            const data = await fetch(Linkuri.backend + Linkuri.artist + Linkuri.picturiArtist + numeAutor);
            const json = await data.json();
            setOpere(json.list)
            console.log(json.list)


        }

        const obtineArtist = async () => {
            const data = await fetch(Linkuri.backend + Linkuri.artist + "/" + numeAutor);
            const json = await data.json()
            setArtist(json)
        }

        obtinePicturi().catch(() => eroareSistem())
        obtineArtist();
        setLoad(false)
    }, [numeAutor,toggle])

    return (
        <Box flex={5}>
            <p style={stilTitlu}>Pagina Artist {artist?.nume}</p>

            <br />
            <CardInfoAutor {...artist!}
            />
            <EditareArtist numeAutor={numeAutor} />
            <Typography sx={{color:"red"}}>Lucrari Realizate</Typography>
            <SimpleReactLightbox>
                <SRLWrapper>
                    <Box sx={{ ...imagini }}>
                        {
                           
                            opere.map((opera) => (
                                <Card sx={{...imagine}}>
                                <CardMedia
                                component="img"
                                alt={opera.nume}
                                height="140"
                                image={opera.urlPoza}
                                />
                                <CardActions>
                                    <Button variant="contained" onClick={()=>history("/opera/"+(opera.nume+","+numeAutor))}>Detalii</Button>
                                    {/* Verific Admin  */}
                                    <Button size="small" variant={"contained"}  sx={{  backgroundColor: '#E1C035' }}>Modifica</Button>
                                     <Button size='small' onClick={()=>stergeOpera(opera.id)} variant={"contained"}  sx={{backgroundColor:'#D40909'}}>Sterge</Button>
                                </CardActions>
                            </Card>
                                // <img style={{ ...imagine }} src={opera.urlPoza} alt={opera.nume} onMouseEnter={() => console.log(opera.nume)} />
                            ))
                        }
                    </Box>
                </SRLWrapper>
            </SimpleReactLightbox>
        </Box>
    )
}

const stilTitlu: CSS.Properties = {
    color: "red",
    fontFamily: 'sans-serif',
    fontSize: '40px',
    backgroundColor: "rgba(100, 220, 135, 0.3)",
    boxShadow: '10px 0 10px rgba(100, 220, 135, 0.3)',
    textAlign: "center"
}

const imagini: CSS.Properties = {
    
    display: "grid",
    gap: "0.5rem",
    gridTemplateColumns: "repeat(auto-fill,minmax(450px,1fr))"
}

const imagine: CSS.Properties = {
    width: "100%",
    height: "100%",
    objectFit: "cover"
}

export default InfoAutor;
