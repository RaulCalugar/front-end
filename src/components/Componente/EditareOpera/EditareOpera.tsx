import React,{FC,useState,useEffect} from 'react'
import { Linkuri } from '../../../Utils/Linkuri'
import {IDetaliiOpera,IArtist,Raspunsuri,Icons,raspuns}  from "../../../Utils/Utils"
import { Autocomplete,Box, Button, FormLabel, TextField } from '@mui/material';
import { useCookies } from "react-cookie";
interface IIdOpera{
    numeOpera:string
}

const EditareOpera:FC<IIdOpera>=({numeOpera})=>{


    const [nume,setNume]=useState<string>("")
    const [numeArtist,setNumeArtist]=useState<string>("")
    const [anAparitie,setAnAparitie]=useState<string>("")
    const [descriere,setDescriere]=useState<string>("")
    const [urlOpera,setUrlOpera]=useState<string>("")
    const [artisti,setArtisti]=useState<string[]>([])
    const [opera,setOpera]=useState<IDetaliiOpera>()
    const [cookies,setCookies]=useCookies(['jwt'])

    const editareOpera=async(id:number)=>{

        console.log(id)
        const dto={
            "operaAutor":numeOpera,
            "numeOpera":nume,
            "numeArtist":numeArtist,
            "anAparitie":anAparitie,
            "descriere":descriere,
            "urlOpera":urlOpera
        }
        fetch(Linkuri.backend+Linkuri.opera+Linkuri.editareOpera+id,
            { method: 'PUT',headers: {
                'Content-Type': 'application/json',
                'Authorization': cookies.jwt
              },body:JSON.stringify(dto) }).then(async response => {
                const data = await response.json();
                if(Raspunsuri.SUCCESS===data){
                    raspuns("SUCCES",Icons.succes,"Opera Modificata Cu Succes");
                }
    })
    }

    useEffect(()=>{
        
        fetch(Linkuri.backend + Linkuri.artist + Linkuri.detaliiOpera + "/" + numeOpera).then(res=>res.json()).then(res=>{
            console.log(res)
            setOpera(res)
            setNume(res.nume)
            setNumeArtist(res.autor)
            setAnAparitie(res.anAparitie)
            setDescriere(res.descriere)
            setUrlOpera(res.urlPoza)
        })  
        const totiArtistii=async()=>{
            const data=await fetch(Linkuri.backend+Linkuri.artist+Linkuri.totiArtistii);
            const json=await data.json()
            const temp:string[]=json.map((item:IArtist)=>{
                return item.nume
            })
            console.log(temp)
            setArtisti(temp)
            
        }
        totiArtistii()

    },[])
    return(
        <Box sx={{justifyContent:'center',alignItems:'center'}}>

        
        <Box sx={{width:'700px'}}>
            <FormLabel>Nume Opera</FormLabel>
                    <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                        value={nume}
                        onChange={(ev)=>setNume(ev.target.value)}/>
                    <FormLabel>Artist</FormLabel>
                    <Autocomplete disablePortal value={numeArtist} options={artisti} fullWidth renderInput={(params) => (<TextField {...params}  />)}
                     onChange={(_, value:any) => {
                         setNumeArtist(value);
                     }}
            />
                   
                        <FormLabel>An Aparitie</FormLabel>
                    <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                        value={anAparitie}
                        onChange={(ev)=>setAnAparitie(ev.target.value)}/>
                    <FormLabel>Descriere</FormLabel>
                    <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                        value={descriere}
                        onChange={(ev)=>setDescriere(ev.target.value)}/>
                    <FormLabel>URL Poza</FormLabel>
                    <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                        value={urlOpera}
                        onChange={(ev)=>setUrlOpera(ev.target.value)}/>
                    <div>
                    <img src={urlOpera} alt={opera?.nume} />
                    </div>
                    <Button fullWidth onClick={()=>editareOpera(opera!.id)}>Editare</Button>                    
        </Box>
        </Box>
    )
}


export default EditareOpera;
