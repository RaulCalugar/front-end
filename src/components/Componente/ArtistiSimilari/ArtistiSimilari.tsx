import React,{useState,useEffect} from "react";
import { IArtistiSimilari,picturiArtist,detaliiArtist } from "../../../Utils/Utils";
import { useNavigate } from "react-router-dom";
import { Linkuri } from "../../../Utils/Linkuri";
import {
    Avatar,
    AvatarGroup,
    Box,
    Divider,
    ImageList,
    ImageListItem,
    List,
    ListItem,
    ListItemAvatar,
    ListItemText,
    Typography,
    Rating
  } from "@mui/material";

export default function ArtistiSimilari(){

    const [artisti, setArtisti] = useState<Array<IArtistiSimilari>>([])
    const history=useNavigate()

    useEffect(()=>{
        const artistiSimilari=async()=>{
            const data = await fetch(Linkuri.backend + Linkuri.artist + Linkuri.artistiSimilari);
            const json = await data.json();
            setArtisti(json)
        }

        artistiSimilari()
    },[])
    return(
        <>
              <Box bgcolor="red" flex={2} p={2} sx={{ display: { xs: "none", sm: "block" } }}>
      <Box bgcolor={"yellow"} position="fixed" width={300}>
      
        <Typography variant="h6" fontWeight={100} mt={2}>
          Artisti Similari
        </Typography>
        
        <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
        {
          artisti.map((artist:IArtistiSimilari,index:number)=>
        
           <>
          <ListItem key={artist.nume} alignItems="flex-start">
        
            <ListItemAvatar >
              <Avatar alt="Remy Sharp" src={artist.urlPoza} 
              // Redirectare la alta pagina
                onClick={()=>console.log(artist.urlPoza)}
              />
            </ListItemAvatar>

            <ListItemText 
              onClick={()=>history("/artist/"+artist.nume)} sx={{cursor:'pointer'}}

              primary={
                <React.Fragment>
                   
                  <Typography
                    sx={{ display: 'inline' }}
                    component="span"
                    variant="body2"
                    color="text.primary"
                    onClick={()=>history("/artist/"+artist.nume)}
                  >
                    {artist.nume}
                  </Typography>
                  
                </React.Fragment>}
         
            />

           </ListItem>
           <Divider variant="inset" component="li" />
           </> 
           )}
        </List>
      </Box>
    </Box>
        </>
    )
}