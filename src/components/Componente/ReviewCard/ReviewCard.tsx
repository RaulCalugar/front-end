import React from 'react'
import { Avatar, Box, Button, IconButton, Card, CardContent, CardHeader, CardActions, Rating, Typography } from "@mui/material"
import MoreVertIcon from '@mui/icons-material/MoreVert';
import "./ReviewCard.css"
const RatingComp = () => {
    return (
        <>
            <Rating name="read-only" value={3} readOnly />
        </>
    )
}

export default function ReviewCard() {


    return (
        <>
            {/* <Box ml={10} sx={{width:"90%",backgroundColor:"red"}}>
            <Card sx={{backgroundColor:"red"}}>
        <CardHeader
          avatar={<Avatar>N</Avatar>}
          title="Card Header"
          component={RatingComp}
          
        />
        <CardContent>
          <Typography variant="h3">Card Title</Typography>
        </CardContent>
        
      </Card>
            </Box> */}

            <div className="cardReview">
                <div className="avatar">
                    <img src='https://source.unsplash.com/random' alt=''/>
                    <div className="infoContinut">
                        <div className="infoText">
                            <div className="reviewAutor">
                                <div className="autor">
                                    <h1>Ion Popa</h1>
                                </div>
                                <div className="review">
                                    {<RatingComp/>}
                                </div>
                                <span>Azi</span>
                            </div>
                            
                        </div>
                        <p>Un ghid excelent</p>
                    </div>
                </div>
            </div>
        </>
    )
}

