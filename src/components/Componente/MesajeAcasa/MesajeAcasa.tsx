import React, { useState, useEffect, useRef } from "react";
import { Mesaje } from "../../../Utils/Utils"
import { Slide, Box, Typography } from "@mui/material";


const mesaje: string[] = [Mesaje.TESTE, Mesaje.REVIEWS, Mesaje.OPERE_REVIEWS]

export default function MesajeAcasa() {

    const containerRef = useRef();
    const [show, setShow] = useState(true);
    const [messageIndex, setMessageIndex] = useState(0);
    useEffect(() => {
        setTimeout(() => {
            setShow(false);
        }, 3000);
        const intervalId = setInterval(() => {
            // get next message
            setMessageIndex((i) => (i + 1) % mesaje.length);

            // slide the message in
            setShow(true);

            setTimeout(() => {
                setShow(false);
            }, 3000);
        }, 4000);

        return () => {
            clearInterval(intervalId);
        };
    }, []);


    return (
        <>
            <Box sx={{
                
                justifyContent: "center",
                alignItems: "center",
                padding: "20px 0px 20px 0px",
                overflow: "hidden",
                backgroundColor:"lightblue"
            }}>

            <Typography>{Mesaje.CONT_INREG}</Typography>
            <Typography>{Mesaje.FACILITATI}</Typography>
            <Slide
                direction={show ? "left" : "right"}
                in={show}
                container={containerRef.current}
                timeout={{
                    enter: 500,
                    exit: 100,
                }}>
                <Box display="flex" justifyContent="center" alignItems="center">
                    <p style={{color:"red", fontSize: "1.5rem"}}>{mesaje[messageIndex]}</p>
                    
                </Box>

            </Slide>
            </Box>
        </>
    )
}
