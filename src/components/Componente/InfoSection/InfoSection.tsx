import React, { useState, useEffect } from "react";
import { eroareSistem } from "../../../Utils/Utils";
import { Link } from "react-router-dom";
import { Box, Card, CardContent, CardMedia, Typography, Button, CardActionArea, CardActions } from '@mui/material';
import { ThemeProvider, makeStyles } from '@mui/styles';
import "./InfoSection.css"

const useStyles = makeStyles(() => makeStyles({
    root: {
      backgroundColor: 'rgb(111, 15, 23)',
    },
    navlinks: {
      
      display: "flex"
    },
    logo: {
      cursor: "pointer",
    },
    link: {
      textDecoration: "none",
      color: "white",
      fontSize: "20px",
      
      "&:hover": {
        color: "yellow",
        borderBottom: "1px solid white",
      },
    },
}));

export default function InfoSection() {

    const [pozaURL, setPozaURL] = useState<string>("")
    const [scurtaDescriere, setScurtaDescriere] = useState<string>("")

   

    useEffect(() => {

        // URL prin care iau o opera aleatoare din cadrul muzeului
        fetch("http://localhost:8080/opera/pozaOpera")
            .then(function (body) {
                return body.text(); 
            }).then(function (data) {
                setPozaURL(data)
            }).catch(() => eroareSistem());
        // URL prin care iau descrierea scurta din cadrul muzeului
        fetch("http://localhost:8080/muzeu/scurtaDescriere").then(function (body) {
            return body.text(); 
        }).then(function (data) {
            setScurtaDescriere(data)
        }).catch(() => eroareSistem());


    }, [])

    const classes = useStyles();
    return (

        <>
       
           
            <div className="info-section">
            <div className="container">
            {/* <div> */}
                <h1 className="infoTitlu">Scurta Descriere</h1>
                <div className="info">
                    <img src={pozaURL} alt=""/>
                
                <p className="text">{scurtaDescriere}
                <Link to={"/despre"}> <button className="buton">Detalii</button> </Link>
                </p>
                {/* <Button >Detalii</Button> */}
                {/* <button className="buton">Detalii</button> */}
            </div>
            </div>
            </div>
    
        </>
    )
}


 {/* <Card sx={{ maxWidth: 545,bg:"red"}} >
                <CardActionArea>
                    <CardMedia
                        component="img"
                        height="140"
                        image={pozaURL}
                        alt="green iguana"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div" >
                            Scurta Descriere
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            {scurtaDescriere}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button size="small" color="primary">
                        Share
                    </Button>
                </CardActions>
            </Card> */}