import React, { FC } from "react";
import { IArtist,Icons,raspuns,Raspunsuri } from "../../../Utils/Utils";
import { useNavigate } from "react-router-dom";
import {
    Avatar,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Checkbox,
    IconButton,
    Typography,
    CardActionArea,
    Button,
    Box
} from "@mui/material";
import { useCookies } from "react-cookie";
import Swal from 'sweetalert2'
import { Linkuri } from "../../../Utils/Linkuri";

const CardAutor: FC<IArtist> = ({ id,nume, aniViata, dataNasterii, dataMortii, descriere, urlPoza }) => {

    const history=useNavigate()
    const [cookies,setCookies]=useCookies(["jwt"]);

    const stergeArtist=async(id:number)=>{
        console.log(id)
        Swal.fire({
            title:'Sigur doriti sa stergeti acest artist?',
            icon:'warning',
            showCancelButton:true,
            cancelButtonText:'Anulare',
            confirmButtonText:'Stergere'
        }).then((res)=>{
            if(res.isConfirmed){
                
                fetch(Linkuri.backend+Linkuri.artist+Linkuri.stergereArtist+id, { method: 'DELETE',headers: {
                   
                    'Authorization':  cookies.jwt
                  } })
                .then(async response => {
                    const data = await response.json();
                    if(Raspunsuri.SUCCESS===data){
                        raspuns("SUCCES",Icons.succes,"Artist Sters Cu Succes");
                    }
                   
                })
                
            }
        })
    }

    return (
        <>
        <Box >
            <Card  sx={{ maxWidth: 345, height:560,marginTop:5 }}>
                <CardMedia
                    component="img"
                    alt={nume}
                    height="400"
                    image={urlPoza}
                    onClick={()=>history("/artist/"+nume)}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {nume}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {aniViata}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" variant={"contained"} onClick={()=>history("/artist/"+nume)}  sx={{ color: 'white', backgroundColor: 'blue' }}>Detalii</Button>
                    <Button size="small" variant={"contained"}  sx={{  backgroundColor: '#E1C035' }}>Modifica</Button>
                    <Button size='small' variant={"contained"} onClick={()=>stergeArtist(id)}  sx={{backgroundColor:'#D40909'}} >Sterge</Button>
                </CardActions>
            </Card>
        </Box>
        </>
    )
}

export default CardAutor;