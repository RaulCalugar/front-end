import React, { FC,useState } from "react";
import { IArtistLocal,getImageFromByteArray } from "../../../Utils/Utils";
import {
    Avatar,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Checkbox,
    IconButton,
    Typography,
    CardActionArea,
    Button,
    Box
} from "@mui/material";
import {useNavigate} from "react-router-dom"

interface IData {
    info: IArtistLocal
}

const CardArtistLocal: FC<IData> = ({ info }) => {

    const history=useNavigate()
    const nume:string=(info.nume+info.prenume)

    return (
        <>
            <Box flex={4} p={{ xs: 0, md: 2 }}>
                <Card sx={{ margin: 5 }}>
                    <div style={{ display: "flex",width:"40%", height: "70%" }}>
                        <CardMedia
                            component="img"
                            height="20%"
                            width="30% !important"
                            image={getImageFromByteArray(info.pozaProfil)}
                            alt={info.nume+" "+info.prenume}

                        />
                        <CardContent>
                            <Typography variant="body2" color="text.primary">
                                Nume Artist:{info.nume+" "+info.prenume}
                            </Typography>
                            <Typography variant="body2" color="text.primary">
                                Localitate:{info.localitate+", judetul"+info.judet}
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                                Studii:{info.studii}
                            </Typography>

                            <Typography variant="body2" color="text.secondary">
                                {info.premii}
                            </Typography>
                        </CardContent>
                    </div>
                    <CardActions>
                        <Button onClick={()=>history("/artistLocal/"+nume)} variant='contained' fullWidth >Detalii</Button>
                    </CardActions>

                </Card>
            </Box>
        </>
    )
}

export default CardArtistLocal