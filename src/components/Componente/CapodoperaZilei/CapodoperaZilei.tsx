import React ,{useEffect,useState} from "react";
import CSS from 'csstype';
import { Linkuri } from "../../../Utils/Linkuri";
import {
   
    Card,
    CardContent,
    CardHeader,
    CardMedia,
    Typography,
    Box
} from "@mui/material";
import {useNavigate} from "react-router-dom"

interface ICapodoperaZilei{
    numeOpera:string,
    numeAutor:string,
    aniViataAutor:string,
    descriere:string,
    urlPoza:string
}

export default function CapodoperaZilei() {

    const [capodopera, setCapodopera]=useState<ICapodoperaZilei>()

    const history=useNavigate()

    useEffect(()=>{

        const capodoperaZilei=async()=>{
            const data=await fetch(Linkuri.backend+Linkuri.muzeu+Linkuri.capodoperaZilei)
            const json=await data.json()
            console.log("JSON:\n",json)
            setCapodopera(json)
        }

        capodoperaZilei()
        

    },[])

    return (

        <Box component={"div"} flex={4} p={{ xs: 0, md: 2 }} bgcolor='green'>
            <Card sx={{ margin: 5,backgroundColor:'yellow' }} >
                <CardHeader title="Capodopera Zilei"/>
                <div style={{display:"flex",height:"100%"}}>

                
                <CardMedia
                    component="img"
                    height="20%"
                    width="70% !important"
                    image={capodopera?.urlPoza}
                    alt="Paella dish"
                    
                />
                <CardContent>
                    <Typography variant="body2" color="text.primary" sx={{...style}}>
                        NUME CAPODOPERA: {capodopera?.numeOpera}
                    </Typography>
                    <Typography variant="body2" color="text.secondary" sx={{...style}}  onClick={()=>history("/artist/"+capodopera?.numeAutor )} >
                        AUTOR: {capodopera?.numeAutor}
                    </Typography>
                    <Typography variant="body2" color="text.secondary" >
                        Ani de viata autor: {capodopera?.aniViataAutor}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Info:
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                    {capodopera?.descriere}
                    </Typography>
                </CardContent>
                </div>
                
            </Card>
        </Box>
    )
}

const style:CSS.Properties={
    cursor:'pointer'
}