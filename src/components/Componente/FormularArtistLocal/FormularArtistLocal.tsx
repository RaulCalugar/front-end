import React, { FC, useState, useEffect } from 'react'
import { IOpera, ManipulareModal,Raspunsuri,eroareSistem,raspuns,Icons } from '../../../Utils/Utils'
import { Autocomplete, Button, Dialog, DialogActions, DialogTitle, DialogContent, FormLabel, OutlinedInput, MenuItem, Checkbox, ListItemText, TextField, InputLabel } from '@mui/material'
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { Linkuri } from '../../../Utils/Linkuri'
import { useCookies } from "react-cookie";
import { styled } from '@mui/material/styles';

interface IJudet {
    auto: string,
    nume: string
}

interface ILocalitati {
    nume: string,
    simplu: string,
    comuna: string
}

const Input = styled('input')({
    display: 'none',
});

const FormularArtistLocal: FC<ManipulareModal> = ({ open, setOpen }) => {

    const [nume, setNume] = React.useState<string>("")
    const [prenume, setPrenume] = React.useState<string>("")
    const [email, setEmail] = React.useState<string>("")
    const [numarTelefon, setNumarTelefon] = React.useState<string>("")
    const [parola, setParola] = React.useState<string>("")
    const campObligatoriu: string = "Acest camp este obligatoriu"
    const eroare = { nume: "", prenume: "", email: "", parola: "" }
    const [judet, setJudet] = useState<string>("")
    const [localitate, setLocalitate] = useState<string>("")
    const [descriere, setDescriere] = useState<string>("")
    const [premii, setPremii] = useState<string>("")
    const [studii, setStudii] = useState<string>("")
    const [lucrari, setLucrari] = useState<Array<IOpera>>([])
    const [cookies,setCookies]=useCookies(['jwt'])

    // Judetele din Romania
    const [judete, setJudete] = useState<IJudet[]>([])

    // files
    const [files,setFiles]=useState<string>('')

    // Localitatile unui judet
    const [localitati, setLocalitati] = useState<Array<string>>(["1", "2", "3"])

    const [imagineProfil,setImagineProfil]=useState<string>("")
    const [imaginiOpere,setImaginiOpere]=useState<string[]>([])
    const [auto, setAuto] = useState<string>("")
    const [im,setIm]=useState<Blob>()
    const [ops,setOps]=useState<Blob[]>([])
    const inchidere = (): void => {
        setOpen(false)
    }

    const alegereJudet = (ev: SelectChangeEvent<typeof judet>) => {

        switch (judet) {
            case ev.target.value:
                setJudet("")
                break;
            default:
                setJudet(ev.target.value)
        }

    }

    useEffect(() => {
        let temp: string[] = []
        fetch(Linkuri.localitati + Linkuri.judete).then(function (body) {
            return body.json()
        }).then(function (data) {
            for (var i = 0; i < data.length; i++) {
                temp[i] = data[i].nume
            }
            setJudete(data)

        })
    }, [])


    // useEffect(()=>{
    //     fetch(Linkuri.localitati+Linkuri.orase).then(function(body){
    //         return body.json()
    //     }).then(function(data){
    //         let temp = new Set<string>();
    //       for(var i=0;i<data.length;i++){
    //             temp.add(data[i].auto)
    //       }
    //       const aux:string[]=Array.from(temp.values())
    //       setLocalitati(aux);
    //     })
    // },[judet])

    const selectareImagineOpere=(ev:any)=>{
        console.log(ev.target.files)
        let temp:string[]=[]
        setFiles(ev.target.files)
        for(var i=0;i<ev.target.files.length;i++){
            temp.push(ev.target.files[i].name)

        }
        setImaginiOpere(temp)
        setOps(ev.target.files[0])
    }

    const selectareImagineProfil=(ev:any)=>{
        console.log(ev.target.files[0].name)
        setImagineProfil(ev.target.files[0].name)
        setIm(ev.target.files[0])
    }
    
    const adaugareArtist=async(ev:React.ChangeEvent<HTMLInputElement>)=>{
        ev.preventDefault()
        const dto={
            "nume":nume,
            "prenume":prenume,
            "email":email,
            "parola":parola,
            "numarTelefon":numarTelefon,
            judet,
            localitate,
            descriere,
            studii,
            premii,
            "pozaProfil":imagineProfil,
            "opere":imaginiOpere
        }

        setOpen(false)
        let formData:FormData =new FormData()
        formData.append("dto",JSON.stringify(dto))
        formData.append("imagineProfil",im!)
        for(let i=0;i<files.length;i++){
            formData.append(`opere`,files[i])
        }
        // formData.append("opere",JSON.stringify(ops))
        
        const res = await fetch(Linkuri.backend + Linkuri.utilizator + Linkuri.inregArtistLocal, {
            body: formData,
            method: "post",
            headers: {
              'Authorization': cookies.jwt
            }
          })
          const data=await res.json()

          switch(data){
            case(Raspunsuri.NUME):
                raspuns("Atentie",Icons.warning,"Mai exista un artist cu acest nume. Va rugam introduceti alte date")
                break;
            case(Raspunsuri.EMAIL):
                raspuns("Atentie",Icons.warning,"Mai exista un artist cu acest nume. Va rugam sa introduceti alta adresa de mail")
                break;
            case(Raspunsuri.TELEFON):
                raspuns("Atentie",Icons.warning,"Mai exista un artist cu acest numar de telefon. Va rugam introduceti alt numar de telefon.")
                break;
            case(Raspunsuri.SUCCESS):
                raspuns("Felicitari! Cont creat cu succss!",Icons.succes,"Bun venit la Muzart, platforma ideala pentru a va promova propriile lucrari de arta")
                break;
            default:
                eroareSistem();
                break;
          }


      
    }

    return (

        <Dialog fullWidth maxWidth="sm" open={open} onClose={inchidere}>
            <DialogTitle>Adaugare Artist Local</DialogTitle>
            <DialogContent>
                
                <TextField autoFocus margin='dense' type='text' label='Nume' fullWidth variant='outlined' value={nume} onChange={(ev) => setNume(ev.target.value)} />
               
                <TextField autoFocus margin='dense' type='text' label='Prenume' fullWidth variant='outlined' value={prenume} onChange={(ev) => setPrenume(ev.target.value)} />
                
                <TextField autoFocus margin='dense' type='text' label='Email' fullWidth variant='outlined' value={email} onChange={(ev) => setEmail(ev.target.value)} />
                
                <TextField autoFocus margin='dense' type='password' label='Parola' fullWidth variant='outlined' value={parola} onChange={(ev) => setParola(ev.target.value)} />

                <TextField autoFocus margin='dense' type='text' label='Numar Telefon' fullWidth variant='outlined' value={numarTelefon} onChange={(ev) => setNumarTelefon(ev.target.value)} />
                <InputLabel id="demo-multiple-checkbox-label">Judet</InputLabel>
                <Select
                    labelId="demo-multiple-checkbox-label"
                    id="demo-multiple-checkbox"
                    value={judet}
                    fullWidth
                    onChange={(ev: any) => {
                        alegereJudet(ev);
                        setAuto(ev.target.value.auto)

                    }}
                    input={<OutlinedInput label="Tag" />}
                    renderValue={(selected) => selected}
                    MenuProps={MenuProps}
                >
                    {judete.map((name) => (
                        <MenuItem key={name.auto} value={name.nume}>
                            <Checkbox inputProps={{ 'aria-label': 'controlled' }} />
                            <ListItemText primary={name.nume} />
                        </MenuItem>
                    ))}
                </Select>
                <br/>
                <br/>
                <Autocomplete  disablePortal fullWidth options={localitati} renderInput={(params) => <TextField  {...params}  label='Localitate'/>} 
                    onChange={(_, value:any) => {
                        setLocalitate(value)
                    }}
                />
                
                <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                    multiline rows={5}
                    label='Descriere'
                    value={descriere}
                    onChange={(ev) => setDescriere(ev.target.value)}
                />
               
                <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                    multiline rows={5}
                    value={studii}
                    label='Studii'
                    onChange={(ev) => setStudii(ev.target.value)}
                />
                <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                    multiline rows={5}
                    label='Premii'
                    value={premii}
                    onChange={(ev) => setPremii(ev.target.value)}
                />
                <label htmlFor="poza-profil">
                    <FormLabel>Poza Profil</FormLabel>
                    <Input accept="image/*" id="poza-profil"  type="file" style={{ width: "100%" }} onChange={(ev)=>selectareImagineProfil(ev)} />
                    <Button variant="contained" component="span" fullWidth>
                        Incarca Poza Profil
                    </Button>
                </label>
                
                <label htmlFor="opere">
                    <FormLabel>Opere</FormLabel>
                    <Input accept="image/*" id="opere" multiple type="file" style={{ width: "100%" }} onChange={(ev)=>selectareImagineOpere(ev)} />
                    <Button variant="contained" component="span" fullWidth>
                        Incarca Opere
                    </Button>
                </label>


            </DialogContent>
            <DialogActions>
                <Button onClick={(ev:any)=>adaugareArtist(ev)} variant="contained" color="success">Salvare Schimbari</Button>
                <Button variant='contained' color='secondary'>Anulare</Button>
            </DialogActions>
        </Dialog>
    )




}

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250
        }
    }
};

export default FormularArtistLocal