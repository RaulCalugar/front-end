import React,{FC,useState,useEffect} from "react";
import { IArtist } from "../../../Utils/Utils";
import CloseIcon from '@mui/icons-material/Close';
import SearchIcon from '@mui/icons-material/Search';
import {Box,TextField,List,ListItem,Divider,ListItemText,ListItemAvatar,Avatar,Typography} from "@mui/material"
import CSS from 'csstype';

interface IArtistProps{
    artisti:IArtist[]
}


const BaraCautare:FC<IArtistProps>=({artisti})=>{


    const [rezultat,setRezultat]=useState<Array<IArtist>>([])
    const [text,setText]=useState<string>("")

    useEffect(()=>{
        if(text===""){
            setRezultat([])
            return
        }
        let temp=artisti.filter((artist:IArtist)=>{
            return artist.nume.toLowerCase().includes(text.toLowerCase())
        })
        
        console.log(temp)
        // temp=temp.map(item=>{
        //     return item.nume.substr(0,item.nume.indexOf("!"))
        // })
        setRezultat(temp)
        
    },[text])

    const stergereFiltru=():void=>{
        setRezultat([])
        setText("")
    }
    return(
        <>
            
            <Box>
                <Box sx={{...atributeCautare}}>
                    <TextField sx={{...inputCautare}} value={text} onChange={(ev)=>setText(ev.target.value)}/>
                    <Box sx={{...iconCautare}}>
                        {
                            rezultat.length===0 ? (
                                <SearchIcon/> 
                            ):(<CloseIcon sx={{cursor:"pointer"}}  onClick={()=>stergereFiltru()}/>)
                        }
                    </Box>
                </Box>
                {
                    rezultat.length!=0 && (
                        <Box sx={{...rezultate}}>
                            <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
                            {
                                rezultat.map((artist)=>{
                                    return(
                                        <>
                                        <ListItem alignItems="flex-start">
                                            <ListItemAvatar>
                                                <Avatar alt={artist.nume} src={artist.urlPoza}/>
                                            </ListItemAvatar>
                                            <ListItemText
                                                primary={
                                                    <React.Fragment>
                                                    <Typography
                                                    sx={{ display: 'inline' }}
                                                    component="span"
                                                    variant="body2"
                                                    color="text.primary"
                                                  >{artist.nume}</Typography>
                                                  </React.Fragment>
                                                }
                                            />
                                        </ListItem>
                                        <Divider variant="inset" component="li" />
                                        </>
                                    )
                                })
                            }
                            </List>
                        </Box>

                    )
                }
            </Box>
        </>
    )
}

const atributeCautare: CSS.Properties = {
    marginTop:"5px",
    marginLeft:"50%",
    display:"flex",
  };

const inputCautare:CSS.Properties={
    border:'0',
    borderRadius:'2px',
    borderTopRightRadius:'0px',
    borderBottomRightRadius:'0px',
    fontSize:"20px",
    padding:'15px',
    width:"300px",
    height:"30px",
}

const iconCautare:CSS.Properties={
    height:"60px",
    marginTop:"13px",
    marginLeft:"1px",
    width:"50px",
    backgroundColor:"white",
    display:"grid",
    placeItems:"center"
}

const rezultate:CSS.Properties={
    marginTop:"5px",
    width:"300px",
    height:"200px",
    backgroundColor:"red",
    boxShadow:"rgba(0, 0, 0, 0.35) 0px 5px 15px",
    overflow: "hidden",
    overflowY:"auto"
}

export default BaraCautare