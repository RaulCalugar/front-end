import React, { FC, useState, useEffect } from 'react'
import { Linkuri } from '../../../Utils/Linkuri'
import { Autocomplete, Box, Button, FormLabel, TextField } from '@mui/material';
import {IArtist,Nationalitate,TipArtist,Icons,Raspunsuri,raspuns} from "../../../Utils/Utils";
import { useCookies } from "react-cookie";

interface IIdArtist {
    numeAutor: string
}

const EditareArtist: FC<IIdArtist> = ({ numeAutor }) => {

    const [id,setId]=useState<number>(0)
    const [nume, setNume] = useState<string>("")
    const [dataNasterii, setDataNasterii] = useState<string>("")
    const [dataMortii, setDataMortii] = useState<string>("")
    const [descriere, setDescriere] = useState<string>("")
    const [urlArtist, setUrlArtist] = useState<string>("")
    const [nationalitate,setNationalitate]=useState<string>("")
    const [tip,setTip]=useState<string>("")
    const [cookies,setCookies]=useCookies(['jwt'])

    useEffect(() => {
        const obtineArtist = async () => {
            const data = await fetch(Linkuri.backend + Linkuri.artist + "/" + numeAutor);
            const json = await data.json()
            setNume(json.nume)
            setId(json.id)
            setDataNasterii(json.dataNasterii)
            setDataMortii(json.dataMortii)
            setDescriere(json.descriere)
            setUrlArtist(json.urlPoza)

        }

        obtineArtist()
       

    }, [])

    const editareArtist=async(id:number)=>{

        const dto={
            nume,
            dataNasterii,
            dataMortii,
            descriere,
            urlArtist,
            nationalitate,
            tip
            
        }
        fetch(Linkuri.backend+Linkuri.artist+Linkuri.editareArtist+id,
            { method: 'PUT',headers: {
                'Content-Type': 'application/json',
                'Authorization': cookies.jwt
              },body:JSON.stringify(dto) }).then(async response => {
                const data = await response.json();
                if(Raspunsuri.SUCCESS===data){
                    raspuns("SUCCES",Icons.succes,"Opera Modificata Cu Succes");
                }
    })
    }

    return (
        <>
            <Box sx={{ justifyContent: 'center', alignItems: 'center',marginLeft:'50px' }}>
                <Box sx={{ width: '700px' }}>
                    <FormLabel>Nume Artist</FormLabel>
                    <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                        value={nume}
                        onChange={(ev) => setNume(ev.target.value)} />
                    <FormLabel>Data Nasterii</FormLabel>
                    <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                        value={dataNasterii}
                        onChange={(ev) => setDataNasterii(ev.target.value)} />

                    <FormLabel>Data Mortii</FormLabel>
                    <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                        value={dataMortii}
                        onChange={(ev) => setDataMortii(ev.target.value)} />
                    <FormLabel>Descriere</FormLabel>
                    <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                        value={descriere}
                        multiline
                        onChange={(ev) => setDescriere(ev.target.value)} />



                    <Autocomplete disablePortal value={nationalitate} options={['a','b']} fullWidth renderInput={(params) => (<TextField {...params} />)}
                        onChange={(_, value: any) => {
                            setNationalitate(value);
                        }}
                    />


                    <FormLabel>URL Poza</FormLabel>
                    <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                        value={urlArtist}
                        onChange={(ev) => setUrlArtist(ev.target.value)} />
                    <div>
                        <img src={urlArtist} alt={nume} />
                    </div>
                    <Button fullWidth onClick={() => editareArtist(id)}>Editare</Button>
                </Box>

            </Box>
        </>
    )
}

export default EditareArtist