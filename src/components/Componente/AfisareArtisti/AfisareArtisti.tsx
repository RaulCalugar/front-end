import React,{FC,useState} from 'react'
import { IArtist } from "../../../Utils/Utils";
import {Box,Button,Grid} from "@mui/material"
import CardAutor from "../CardAutor/CardAutor"
import TravelExploreIcon from '@mui/icons-material/TravelExplore';


interface IArtistData {
    artisti: IArtist[],
    total:number,
    limita: number,
    afisare:number
}

const AfisareArtisti:FC<IArtistData>=({artisti,total,limita,afisare})=>{

    const [lim,setLim]=useState<number>(afisare)


    return(
        <Box>
             <Box 
                sx={{
                display: "flex",
                gap: "20px",
                flexWrap: "wrap",
                margin: "auto",
                width: "90vw",
                marginTop:"30px"
            }}
           >
            {
                artisti.slice(0,lim).map((item:IArtist)=>(
                    console.log("ID:",item.id)
                ))
            }
            <Grid container spacing={4} columnSpacing={4}>
            {
                artisti.slice(0,lim).map((artist:IArtist)=>
                    <Grid item xs={2} sm={4} md={4} key={artist.urlPoza}>
                    <CardAutor key={artist.nume} {...artist!}/>
                    </Grid>
                )
                
            }
            </Grid>
            </Box>
           
                {
                    artisti.length > lim ? (
                        <Box
                        border={1}
                        borderColor="red"
                        width="70%"
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                        bgcolor="yellow"
                        margin={"30px auto"}
                        color="white"
                        textAlign={"center"}
                        fontSize={24}
                    >
                        <Button onClick={()=>setLim(lim+limita)} sx={{width:"100%"}} variant="contained" >1-{lim} din {total}  {<TravelExploreIcon/>} {<span > Vedeti Mai Mult</span>}</Button>
                        </Box>
                    ) : (null)
                }
        </Box>
    )
}

export default AfisareArtisti;