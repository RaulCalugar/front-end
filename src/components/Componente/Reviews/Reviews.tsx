import React, { useEffect,useState } from "react";
import { Linkuri } from "../../../Utils/Linkuri";
import { IReviewData , getImageFromByteArray } from "../../../Utils/Utils";
import {
  Avatar,
  Box,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
  Rating
} from "@mui/material";




export default function Reviews() {

  const [reviews, setReviews] = useState<Array<IReviewData>>([])
  const [imagini, setImagini] = useState<Array<string>>([])

  const temp=(json:IReviewData[]):string[]=>{
      let img:Array<string>=[];
      for(var i=0;i<json.length;i++){
        img[i]=getImageFromByteArray(json[i])
      }
      return img;
  }
  useEffect(()=>{
    

    const obtinereReviews = async () => {
        const data = await fetch(Linkuri.backend + Linkuri.muzeu + Linkuri.reviews);
        const json = await data.json();
        setReviews(json)
        setImagini(temp(json))
      }

      obtinereReviews();

    
  },[])


  return (
    <Box bgcolor="red" flex={2} p={2} sx={{ display: { xs: "none", sm: "block" } }}>
      <Box bgcolor={"yellow"} position="fixed" width={300}>
      
        <Typography variant="h6" fontWeight={100} mt={2}>
          Parerile Utilizatorilor
        </Typography>
        
        <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
        {
          reviews.map((review:IReviewData,index:number)=>
           <>
          <ListItem key={review.utilizator} alignItems="flex-start">
        
            <ListItemAvatar key={review.continut}>
              <Avatar alt="Remy Sharp" src={review.imagineProfil} />
            </ListItemAvatar>

            <ListItemText key={review.utilizator}
              

              primary={
                <React.Fragment>
                  <Rating name="read-only" value={review.nota} readOnly />
                  <br />
                  <Typography
                    sx={{ display: 'inline' }}
                    component="span"
                    variant="body2"
                    color="text.primary"
                  >
                    {review.utilizator}
                    <br/>
                  </Typography>
                  {review.continut}
                </React.Fragment>}
         
            />

           </ListItem>
           </> 
           )}
          
         
        </List>
      </Box>
    </Box>
  );
};


