import React from 'react'

import Slider,{ Settings } from "react-slick";

function SampleNextArrow() {
    
    return (
      <div
        style={{  display: "block", background: "red" }}
        
      />
    );
  }
function SamplePrevArrow() {
   
    return (
      <div
        
        style={{ display: "block", background: "green" }}
       
      />
    );
  }

export default function Carusel() {
    const settings:Settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />
      
      };
      return (
        <div style={{width:'1000px',height:'300px'}}>
          <Slider {...settings}>
            <div>
              <img style={{height:'220PX'}} src="https://source.unsplash.com/random" alt='' />
            </div>
            <div>
            <img style={{height:'220PX'}} src="https://source.unsplash.com/random" alt='' />
            </div>
            <div>
            <img style={{height:'220PX'}} src="https://source.unsplash.com/random" alt='' />
            </div>
            <div>
            <img style={{height:'220PX'}} src="https://source.unsplash.com/random" alt='' />
            </div>
            <div>
            <img style={{height:'220PX'}} src="https://source.unsplash.com/random" alt='' />
            </div>
            <div>
            <img style={{height:'220PX'}} src="https://source.unsplash.com/random" alt='' />
            </div>
            <div>
            <img style={{height:'220PX'}} src="https://source.unsplash.com/random" alt='' />
            </div>
            <div>
            <img style={{height:'220PX'}} src="https://source.unsplash.com/random" alt='' />
            </div>
            <div>
            <img style={{height:'220PX'}} src="https://source.unsplash.com/random" alt='' />
            </div>
          </Slider>
        </div>
      );
}