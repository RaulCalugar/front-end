import React, { useState, useEffect,useRef } from "react"

import { Linkuri } from "../../../Utils/Linkuri"
import { IOpera } from "../../../Utils/Utils"
import Slick,{ Settings } from "react-slick";
import Slider from "react-slick";
import CardEchipa from "../CardEchipa/CardEchipa";
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
// import Carousel from 'better-react-carousel'
import Carousel from 'better-react-carousel'

export default function OpereAleatoare() {

    

    const [opere, setOpere] = useState<IOpera[]>([])
    const [nav1, setNav1] = useState();
  const [nav2, setNav2] = useState();
  const slider1 = useRef(null);
  const slider2 = useRef(null);

    useEffect(() => {
        
        const obtineOpere = async () => {
            const data = await fetch(Linkuri.backend + Linkuri.muzeu+Linkuri.opereAleatoare);
            const json = await data.json()
            setOpere(json)
        }
      
        obtineOpere()
    }, [])

    const settings:Settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1
      };
      return (
        <div>
      
        <Carousel >
                {
                    opere.slice(0,10).map((opera:IOpera)=>(
                      <Carousel.Item>
                        <div style={{width:'100%'}}>
                            <img src={opera.urlPoza} style={{width:'100%'}}/>
                        </div>
                        </Carousel.Item>
                        
                    ))
                }
          </Carousel>
      
        </div>
      );
    
}