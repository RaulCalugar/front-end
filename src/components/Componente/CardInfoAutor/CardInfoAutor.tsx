import React, { FC } from "react";
import { IArtist } from "../../../Utils/Utils";
import {
    Avatar,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Checkbox,
    IconButton,
    Typography,
    CardActionArea,
    Button,
    Box
} from "@mui/material";



const CardInfoAutor: FC<IArtist> = ({ nume, aniViata, dataNasterii, dataMortii, descriere, urlPoza }) => {

    return (
        <>
            <Box flex={4} p={{ xs: 0, md: 2 }}>
                <Card sx={{ margin: 5 }}>
                    <CardHeader title={nume} />
                    <div style={{ display: "flex", height: "100%" }}>


                        <CardMedia
                            component="img"
                            height="20%"
                            width="70% !important"
                            image={urlPoza}
                            alt={nume}

                        />
                        <CardContent>
                            <Typography variant="body2" color="text.primary">
                                Nume Artist: {nume}
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                                Data Nasterii : {dataNasterii}
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                                Data Mortii : {dataMortii}
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                                Ani de viata autor: {aniViata}
                            </Typography>
                            
                            <Typography variant="body2" color="text.secondary">
                                {descriere}
                            </Typography>
                        </CardContent>
                    </div>

                </Card>
            </Box>
        </>
    )
}

export default CardInfoAutor;