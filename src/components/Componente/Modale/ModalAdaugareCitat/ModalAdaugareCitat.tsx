import React,{FC,useState,useEffect} from 'react'
import { ManipulareModal,Raspunsuri,raspuns,Icons } from '../../../../Utils/Utils'
import {Autocomplete,Button,Dialog,DialogTitle,DialogContent,DialogActions,Typography,TextField, FormLabel} from "@mui/material"
import { Linkuri } from '../../../../Utils/Linkuri'
import { useCookies } from "react-cookie";


const ModalAdaugareCitat: FC<ManipulareModal>=({open,setOpen})=>{

    const [autor,setAutor] = useState<string>("")
    const [autorExistent, setAutorExistent]=useState<string>("")
    const [text,setText] = useState<string>("")
    const [urlPoza,setUrlPoza] = useState<string>("")
    const [autori,setAutori]=useState<Array<string>>([])
    const [cookie,setCookies]=useCookies(['jwt'])
    
    const inchidere = (): void => {
        setOpen(false)
    }

    useEffect(()=>{
        const autoriCitate=async()=>{
            const data=await fetch(Linkuri.backend+Linkuri.muzeu+Linkuri.autoriCitate,{
                method:"GET",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': cookie.jwt
                  }
            })
            const json=await data.json()
            setAutori(json)
        }

        autoriCitate();
    },[])

    const adaugareCitat=async(ev:React.ChangeEvent<HTMLInputElement>)=>{
        ev.preventDefault()
        const citatDto={
            "numeArtist":autor,
            "urlPozaArtist":urlPoza,
            "text":text
        }
        const res=await fetch(Linkuri.backend+Linkuri.citat+Linkuri.adaugareCitat,{
            body:JSON.stringify(citatDto),
            method: "post",
            headers: {
              'Content-Type': 'application/json',
              'Authorization': cookie.jwt
            }
        })
        const temp = await res.json()
        if(Raspunsuri.CITAT_EXISTENT===temp){
            setOpen(false)
            raspuns("ATENTIE",Icons.warning,"Exista deja un citat cu un astfel de nume")
        }
        else if(Raspunsuri.SUCCESS===temp){
            setOpen(false)
            raspuns("FELICITARI",Icons.succes,"Citat adaugat cu succes!")
        }
    }
    return(
        <Dialog fullWidth
        maxWidth="sm" open={open} onClose={inchidere}
        >
        <DialogTitle>Adauga Review</DialogTitle>
        <DialogContent>
            <FormLabel>Nume Artist</FormLabel>
            <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                value={autor} onChange={(e)=>{
                    setAutor(e.target.value);
                    setAutorExistent("")
                }}/>
            <FormLabel>Artisti Existenti</FormLabel>
            <Autocomplete disablePortal value={autorExistent} options={autori} fullWidth renderInput={(params) => (<TextField {...params} label="Artisti Existenti" />)}
                 onChange={(_, value:any) => {
                    setAutor(value);
                    setAutorExistent(value);
                }}
            />

            <FormLabel>URL Artist</FormLabel>
            <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                value={urlPoza} onChange={(e)=>setUrlPoza(e.target.value)}/>

            <FormLabel>Text Citat</FormLabel>
                <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                    multiline rows={5}
                    value={text}
                    onChange={(ev)=>setText(ev.target.value)}
                />
        </DialogContent>
        <DialogActions>
            <Button onClick={()=>inchidere()}>Anulare</Button>
            <Button onClick={(ev:any)=>adaugareCitat(ev)}>Adaugare Citat</Button>
        </DialogActions>
        </Dialog>
    )
}

export default ModalAdaugareCitat;
