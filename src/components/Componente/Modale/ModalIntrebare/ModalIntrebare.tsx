import React,{useState,FC} from 'react'
import { ManipulareModal,ICurent,raspuns,Raspunsuri,Icons,eroareSistem,IIntrebare } from "../../../../Utils/Utils";
import { Linkuri } from '../../../../Utils/Linkuri';
import { Button, Dialog, DialogTitle,DialogContent,DialogActions,TextField,FormLabel } from '@mui/material';
import { useCookies } from "react-cookie";

const ModalIntrebare:FC<ManipulareModal>=({open,setOpen})=>{

    const [enunt,setEnunt]=useState<string>("")
    const [raspunsCorect,setRaspunsCorect]=useState<string>("")
    const [raspunsGresit1,setRaspunsGresit1]=useState<string>("")
    const [raspunsGresit2,setRaspunsGresit2]=useState<string>("")
    const [raspunsGresit3,setRaspunsGresit3]=useState<string>("")
    const [imagine,setImagine]=useState<string>("")
    const [cookie,setCookie]=useCookies(['jwt'])

    const inchidere = (): void => {
        setEnunt("")
        setRaspunsCorect("")
        setRaspunsGresit1("")
        setRaspunsGresit2("")
        setRaspunsGresit3("")
        setOpen(false)
    }

    const adaugareIntrebare=async(ev:React.ChangeEvent<HTMLInputElement>)=>{

        ev.preventDefault()
        const dto:IIntrebare={
            enunt,
            raspunsCorect,
            raspunsGresit1,
            raspunsGresit2,
            raspunsGresit3,
            imagine
        }

        const res = await fetch(Linkuri.backend + Linkuri.intrebare + Linkuri.inserareIntrebare, {
            body: JSON.stringify(dto),
            method: "post",
            headers: {
              'Content-Type': 'application/json',
              'Authorization': cookie.jwt
            }
          })

        const temp=await res.json()
        switch(temp){
            case Raspunsuri.SUCCESS:
                raspuns('Succes',Icons.succes,"Intrebare adaugata cu succes!")
                inchidere()
                setOpen(true)
                break;
            case(Raspunsuri.GASIT):
                inchidere()
                raspuns("ATENTIE",Icons.warning,"Mai exista intrebare cu un astfel de rasuns!")
                setOpen(true)
                break;
            default:
                inchidere();
                eroareSistem();
                break;
        }

      


    }

    return(

        <Dialog fullWidth maxWidth="sm" open={open} onClose={inchidere}>
        <DialogTitle>Adaugati Intrebare</DialogTitle>
        <DialogContent>

            <FormLabel>Enuntul Intrebarii</FormLabel>
            <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                value={enunt} onChange={(e)=>setEnunt(e.target.value)}/>
            <FormLabel>Raspuns Corect</FormLabel>
            <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                value={raspunsCorect} onChange={(e)=>setRaspunsCorect(e.target.value)}/>
            <FormLabel>Raspuns Gresit 1</FormLabel>
            <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                value={raspunsGresit1} onChange={(e)=>setRaspunsGresit1(e.target.value)}/>
              <FormLabel>Raspuns Gresit 2</FormLabel>
            <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                value={raspunsGresit2} onChange={(e)=>setRaspunsGresit2(e.target.value)}/>
              <FormLabel>Raspuns Gresit 3</FormLabel>
            <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                value={raspunsGresit3} onChange={(e)=>setRaspunsGresit3(e.target.value)}/>
            <FormLabel>Imagine</FormLabel>
            <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                value={imagine} onChange={(e)=>setImagine(e.target.value)}/>

            
        </DialogContent>
        <DialogActions>
            <Button  onClick={inchidere}>Anulati</Button>
            <Button  onClick={(event: any) => { adaugareIntrebare(event) }}>Adaugare Intrebare</Button>
        </DialogActions>
        </Dialog>
    )
}

export default ModalIntrebare