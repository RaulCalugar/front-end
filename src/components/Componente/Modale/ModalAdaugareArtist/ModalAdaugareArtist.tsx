import React,{useState , FC} from 'react'

import { Avatar, Button,Dialog,DialogTitle,DialogContent,DialogActions,Typography, TextField, FormLabel } from '@mui/material';
import { ManipulareModal, IOpera, Raspunsuri, Icons,raspuns } from '../../../../Utils/Utils';
import ModalOpere from '../ModalOpere/ModalOpere';
import { Linkuri } from '../../../../Utils/Linkuri';
import { useCookies } from "react-cookie";

const ModalAdaugareArtist: FC<ManipulareModal> = ({ open, setOpen })=>{

    const [nume, setNume] = useState<string>("")
    const [dataNasterii,setDataNasterii] = useState<string>("")
    const [dataMortii,setDataMortii] = useState<string>("")
    const [pozaArtist,setPozaArtist]= useState<string>("")
    const [biografie, setBiografie] = useState<string>("")
    const [adaugareOpere,setAdaugareOpere]=useState<boolean>(false)
    const [opere,setOpere] = useState<Array<IOpera>>([])
    const [cookie,setCookies]=useCookies(['jwt'])

    const inchidere=():void=>{
        setNume("")
        setDataNasterii("")
        setDataMortii("")
        setBiografie("")
        setOpere([])
        setOpen(false)
    }

    const adaugareArtist=async(ev:React.ChangeEvent<HTMLInputElement>)=>{

        ev.preventDefault()
        const opereDto={
            "status":Raspunsuri.SUCCESS,
            "list":opere
        }
        const artistDto={
            status:Raspunsuri.SUCCESS,
            nume:nume,
            dataNasterii,
            dataMortii,
            "descriere":biografie,
            "dto":opereDto
        }
        setOpen(false)
        const res = await fetch(Linkuri.backend + Linkuri.artist + Linkuri.adaugareArtist, {
            body: JSON.stringify(artistDto),
            method: "post",
            headers: {
              'Content-Type': 'application/json',
              'Authorization': cookie.jwt
            }
          })
      
          
          const temp = await res.json()
          if(Raspunsuri.SUCCESS===temp){
                raspuns("Felicitari",Icons.succes,"Artist Adaugat cu Succes")
          }
          else if(Raspunsuri.AUTOR_GASIT===temp){
            raspuns("Atentie",Icons.warning,"Exista in baza de date a sistemului un artist cu acest nume")
          }
    
    }
    return(
        <>
            <Dialog fullWidth
              maxWidth="sm" open={open} onClose={inchidere}
            >
            <DialogTitle>Adaugare Artist</DialogTitle>
            <DialogContent>
                <FormLabel>Nume</FormLabel>
                <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                    value={nume}
                    onChange={(ev)=>setNume(ev.target.value)}
                />
                <FormLabel>Data Nasterii</FormLabel>
                <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                    value={dataNasterii}
                    onChange={(ev)=>setDataNasterii(ev.target.value)}
                />
                <FormLabel>Data Mortii</FormLabel>
                <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                    value={dataMortii}
                    onChange={(ev)=>setDataMortii(ev.target.value)}
                />
                <FormLabel>URL Poza</FormLabel>
                <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                    value={pozaArtist}
                    onChange={(ev)=>setPozaArtist(ev.target.value)}
                />
                {/* <img src="https://source.unsplash.com/random" alt=""/> */}

                <FormLabel>Biografie</FormLabel>
                <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                    multiline rows={5}
                    value={biografie}
                    onChange={(ev)=>setBiografie(ev.target.value)}
                />
                <Button variant="outlined" fullWidth>Selectare Opere</Button>
            </DialogContent>
            <DialogActions>
                <Button onClick={()=>inchidere()}>Anulare</Button>
                <Button onClick={()=>setAdaugareOpere(true)}>Setare Opere</Button>
                <Button onClick={(ev:any)=>adaugareArtist(ev)}>Adaugare Artist</Button>
            </DialogActions>
            </Dialog> 
            <ModalOpere open={adaugareOpere} setOpen={setAdaugareOpere} opere={opere}/>
        </>
    )
}

export default ModalAdaugareArtist;

