import React, { useState, FC } from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Typography from '@mui/material/Typography';
import { TextField, Rating, FormLabel } from '@mui/material';
import { ManipulareModal } from "../../../../Utils/Utils";
import { Linkuri } from '../../../../Utils/Linkuri';
import { Raspunsuri,eroareSistem,raspuns,Icons } from '../../../../Utils/Utils';
import { useCookies } from "react-cookie";




const ModalAdaugareReview: FC<ManipulareModal> = ({ open, setOpen }) => {
    const [continut, setContinut] = useState<string>("")
    const [nota, setNota] = useState<number | null>(0)
    const [cookie,setCookies]=useCookies(['jwt'])

    const inchidere = (): void => {
        setNota(0)
        setContinut("")
        setOpen(false)
    }

    const adaugareReview=async(ev:React.ChangeEvent<HTMLInputElement>)=>{
        ev.preventDefault()
        const data=new Date()
        
        const dto={
            "continut":continut,
            "nota":nota,
            "data":data.toLocaleDateString(),
            "token":cookie.jwt
        }

        const res = await fetch(Linkuri.backend + Linkuri.review + Linkuri.adaugareReview, {
            body: JSON.stringify(dto),
            method: "post",
            headers: {
              'Content-Type': 'application/json',
              'Authorization': cookie.jwt
            }
          })
      
          
          const temp = await res.json()
          switch(temp){
            case Raspunsuri.SUCCESS:
                inchidere()
                raspuns('Multumim',Icons.succes,"Ne bucuram ca v-ati spus parerea despre aplicatie")
                break;
            default:
                eroareSistem();
                break;
          }
    }

    return (
        <Dialog fullWidth
            maxWidth="sm" open={open} onClose={inchidere}>
            <DialogTitle>Adauga Review</DialogTitle>
            <DialogContent>
                <FormLabel>Impartaseste opinia ta despre acest muzeu</FormLabel>
                <TextField
                    autoFocus
                    multiline
                    rows={5}
                    margin="dense"
                    id="continut"
                    value={continut}
                    onChange={(ev) => setContinut(ev.target.value)}
                    label="Spune parerea ta despre Muzart"
                    type="text"
                    fullWidth
                    variant="outlined"


                />
                <Typography>Acorda o nota acestui site</Typography>
                {/* <FormLabel>Acorda o nota acestui site</FormLabel> */}
                <Rating
                    name="simple-controlled"
                    value={nota}
                    onChange={(event, newValue) => {
                        setNota(newValue);
                    }}
                />
            </DialogContent>
            <DialogActions>
                <Button  onClick={inchidere}>Anulati</Button>
                <Button  onClick={(event: any) => { adaugareReview(event) }}>Adaugare Review</Button>
            </DialogActions>
        </Dialog>
    );
}

export default ModalAdaugareReview

