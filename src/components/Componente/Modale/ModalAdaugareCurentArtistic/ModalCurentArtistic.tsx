import React,{useState,FC} from 'react'
import { ManipulareModal,ICurent,raspuns,Raspunsuri,Icons,eroareSistem } from "../../../../Utils/Utils";
import { Linkuri } from '../../../../Utils/Linkuri';
import { Button, Dialog, DialogTitle,DialogContent,DialogActions,TextField,FormLabel } from '@mui/material';
import { useCookies } from "react-cookie";

const ModalCurentArtistic:FC<ManipulareModal>=({open,setOpen})=>{

    const [nume,setNume]=useState<string>("")
    const [descriere,setDescriere]=useState<string>("")
    const [cookie,setCookie]=useCookies(['jwt'])

    const adaugareCurentArtistic=async(ev:React.ChangeEvent<HTMLInputElement>)=>{
        ev.preventDefault()
        const dto:ICurent={
            nume,
            descriere
        }

        const res = await fetch(Linkuri.backend + Linkuri.curente + Linkuri.adaugareCurent, {
            body: JSON.stringify(dto),
            method: "post",
            headers: {
              'Content-Type': 'application/json',
              'Authorization': cookie.jwt
            }
          })
      
          
          const temp = await res.json()
          switch(temp){
            case Raspunsuri.SUCCESS:
                inchidere()
                raspuns('Succes',Icons.succes,"Curent artistic adaugat cu succes!")
                break;
            case(Raspunsuri.GASIT):
                inchidere();
                raspuns("ATENTIE",Icons.warning,"Mai exist un curent artistic cu acest nune!")
                break;
            default:
                inchidere();
                eroareSistem();
                break;
          }
    }

    const inchidere = (): void => {
        setNume("")
        setDescriere("")
        setOpen(false)
    }
    return(
        <Dialog fullWidth maxWidth="sm" open={open} onClose={inchidere}>
        <DialogTitle>Adauga Curent Artistic</DialogTitle>
        <DialogContent>

            <FormLabel>Nume Curent Artistic</FormLabel>
            <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                value={nume} onChange={(e)=>setNume(e.target.value)}/>
            <FormLabel>Descriere Curent Artistic</FormLabel>
            <TextField
                autoFocus
                multiline
                rows={5}
                margin="dense"
                id="continut"
                value={descriere}
                onChange={(ev) => setDescriere(ev.target.value)}
                label="Descrierea Curentului Artistic"
                type="text"
                fullWidth
                variant="outlined"
            />
            
        </DialogContent>
        <DialogActions>
            <Button  onClick={inchidere}>Anulati</Button>
            <Button  onClick={(event: any) => { adaugareCurentArtistic(event) }}>Adaugare Curent Artistic</Button>
        </DialogActions>
    </Dialog>
    )
}

export default ModalCurentArtistic