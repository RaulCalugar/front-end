import React,{useState , FC} from 'react'

import { ManipulareModal, IOpera, Raspunsuri } from '../../../../Utils/Utils';

import { Dialog,DialogTitle, DialogContent, DialogActions, Typography, Button, FormLabel, TextField } from '@mui/material';

type Props={
    open:boolean;
    setOpen:(open:boolean)=>void;
    opere:IOpera[]

}



const ModalOpere:FC<Props>=({open,setOpen,opere})=>{
    const [nume,setNume] =  useState<string>("")
    const [url,setUrl] = useState<string>("")
    const [adaugat,setAdaugat]=useState<boolean>(false)

    const inchidere = () : void=>{
        setAdaugat(false)
        setOpen(false)        
    }


    
    return(
        <>
            <Dialog fullWidth maxWidth="sm" open={open} onClose={inchidere}>
                <DialogTitle>Adaugare Opere</DialogTitle>
                <DialogContent>
                    <FormLabel>Nume Opera</FormLabel>
                    <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                        value={nume}
                        onChange={(ev)=>setNume(ev.target.value)}/>
                    <FormLabel>URL POZA</FormLabel>
                    <TextField autoFocus margin='dense' type='text' fullWidth variant='outlined'
                        value={url}
                        onChange={(ev)=>{setUrl(ev.target.value);setAdaugat(true)}}/>
                    {
                        adaugat ? (
                            null
                        ):(<img src={url} alt=""/>)
                    }
                </DialogContent>
                <DialogActions>
                    <Button onClick={inchidere}>Inchidere</Button>
                    <Button onClick={()=>opere.push({id:0,nume,urlPoza:url})}>Adaugare Opera</Button>
                </DialogActions>
            </Dialog>
        </>
    )
}

export default ModalOpere;