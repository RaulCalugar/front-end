import React, { useState, useEffect } from 'react'

import { Box, Button, ButtonGroup } from "@mui/material"

import ModalAdaugareArtist from '../Modale/ModalAdaugareArtist/ModalAdaugareArtist'
import ModalAdaugareCitat from '../Modale/ModalAdaugareCitat/ModalAdaugareCitat'
import FormularArtistLocal from '../FormularArtistLocal/FormularArtistLocal'
import ModalCurentArtistic from '../Modale/ModalAdaugareCurentArtistic/ModalCurentArtistic'
import ModalIntrebare from '../Modale/ModalIntrebare/ModalIntrebare'

export default function PanouControl() {

    const [addArtist, setAddArtist]=useState<boolean>(false)
    const [addCitat,setAddCitat]=useState<boolean>(false)
    const [addArtistLocal,setAddArtistLocal]=useState<boolean>(false)
    const [adaugareCurentArtistic,setAdaugareCurentArtistic]=useState<boolean>(false)
    const [intrebare,setIntrebare]=useState<boolean>(false)
    return (
        <Box>
            <ButtonGroup variant="contained" aria-label="outlined primary button group">
                <Button onClick={()=>setAddArtist(true)}>Artist</Button>
                <Button onClick={()=>setAddCitat(true)}>Citat</Button>
                <Button onClick={()=>setAddArtistLocal(true)}>Artist Local</Button>
                <Button onClick={()=>setAdaugareCurentArtistic(true)}>Curent</Button>
                <Button onClick={()=>setIntrebare(true)}>Intrebare</Button>
            </ButtonGroup>
            <ModalAdaugareArtist open={addArtist} setOpen={setAddArtist} />
            <ModalAdaugareCitat open={addCitat} setOpen={setAddCitat} />
            <FormularArtistLocal open={addArtistLocal} setOpen={setAddArtistLocal} />
            <ModalCurentArtistic open={adaugareCurentArtistic} setOpen={setAdaugareCurentArtistic} />
            <ModalIntrebare open={intrebare} setOpen={setIntrebare} />
        </Box>
        
    )
}