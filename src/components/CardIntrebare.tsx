import React, { FC } from "react";
import { Raspuns } from "../PaginaQuiz"
import styled from 'styled-components';
import './CardIntrebare.css'
type Props = {
    intrebare: string;
    raspunsuri: string[];
    callback: (e: React.MouseEvent<HTMLButtonElement>) => void;
    raspunsUtilizator: Raspuns | undefined;
    numarIntrebare: number;
    totalIntrebari: number
}

type Demo={
    intrebare:string,
    raspunsuri:string[],
    callback: (e: React.MouseEvent<HTMLButtonElement>) => void;
    raspunsUtilizator: Raspuns | undefined;
    raspunsCorect:string;
    numarIntrebare: number;
    totalIntrebari: number
}



const CardIntrebare: FC<Demo> = ({ intrebare, raspunsuri, callback, raspunsUtilizator,raspunsCorect, numarIntrebare, totalIntrebari }) => {
    return (
        <div className="card">
            <p className="numar">Intrebarea: {numarIntrebare}/{totalIntrebari}</p>
            <p className="textIntrebare" dangerouslySetInnerHTML={{ __html: intrebare }} />
            <div>
                {raspunsuri.map(raspuns => (
                    <Varianta key={raspuns}
                        corect={raspunsUtilizator?.raspunsCorect === raspuns}
                        selectat={raspunsUtilizator?.raspuns===raspuns}
                    >
                        <button disabled={!!raspunsUtilizator} onClick={callback} value={raspuns}>
                            <span dangerouslySetInnerHTML={{ __html: raspuns }} />
                        </button>
                    </Varianta>
                )
                )}
            </div>

        </div>
    )

}

type ButtonWrapperProps = {
    corect: boolean;
    selectat: boolean;
};

const Varianta = styled.div<ButtonWrapperProps>`
transition: all 0.3s ease;

:hover {
  opacity: 0.8;
}

button {
  cursor: pointer;
  user-select: none;
  font-size: 0.8rem;
  width: 100%;
  height: 40px;
  margin: 5px 0;
  background: ${({ corect, selectat }) =>
        corect
            ? 'linear-gradient(90deg, #56FFA4, #59BC86)'
            : !corect && selectat
                ? 'linear-gradient(90deg, #FF5656, #C16868)'
                : 'linear-gradient(90deg, #56ccff, #6eafb4)'};
  border: 3px solid #ffffff;
  box-shadow: 1px 2px 0px rgba(0, 0, 0, 0.1);
  border-radius: 10px;
  color: #fff;
  text-shadow: 0px 1px 0px rgba(0, 0, 0, 0.25);
}
`
export default CardIntrebare;
