export const Linkuri={
    utilizator: "/utilizator",
    backend:"http://localhost:8080",
    inregistrare:"/inregistrareUtilizator",
    logare:"/logareUtilizator",
    generareCod:"/generareCod",
    verificareCod:"/verificareCod",
    schimbareParola:"/schimbareParola",
    muzeu:"/muzeu",
    descriere:"/descriere",
    scurtaDescriere:"/scurtaDescriere",
    artist:"/artist",
    picturiArtist:"/picturiArtist/",
    review:"/review",
    adaugareReview:"/adaugareReview",
    reviews:"/reviews",
    artistiSimilari:"/artistiSimilari",
    adaugareArtist:"/adaugareArtist",
    opere:"/opere",
    totiArtistii:"/totiArtistii",
    autoriCitate:"/autoriCitate",
    citat:"/citat",
    adaugareCitat:"/adaugareCitat",
    capodoperaZilei:"/capodoperaZilei",
    opereSimilare:"/opereSimilare",
    detaliiOpera:"/detaliiOpera",
    opera:"/opera",
    // Date folosite pentru inregistrarea artistilor locali
    localitati:"https://roloca.coldfuse.io/",
    judete:"judete",
    orase:"orase/",
    inregArtistLocal:"/inregistrareArtistLocal",
    artistLocal:"/artistLocal/",
    toti:"/toti",
    artistiLocali:"/artistiLocali",
    stergereOpera:"/stergereOpera/",
    editareOpera:'/editareOpera/',
    stergereArtist:"/stergereArtist/",
    editareArtist:"/editareArtist/",
    curente:'/curente',
    adaugareCurent:'/adaugare',
    stergereCurent:'/stergere',
    editareCurent:'/editareCurent',

    // Intrebare
    intrebare:'/intrebare',
    inserareIntrebare:'/inserareIntrebare',
    editareIntrebare:'/editareIntrebare',
    intrebariTest:'/intrebariTest',

    // Progres
    inregistrareProgres:'/inregistrareProgres',

    // Opere
    opereAleatoare:'/opereAleatoare',

    // Utilizator
    pozaProfil:'/pozaProfil',
    editareProfil:'/editareProfil',
    detaliiUtilizator:'/detaliiUtilizator'

}

export const operaLinks={
    opera:'/opera',
    pozOpera:"/pozaOpera"
}

