export const amestecaRaspunsuri = (array: any[]) =>
  [...array].sort(() => Math.random() - 0.5);
