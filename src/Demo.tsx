import React from 'react';
import './App.css';
import "./Demo.css"
import {useNavigate} from 'react-router-dom'
import HomePage from './Pagini/HomePage/HomePage';
import {Box, Stack,Grid} from "@mui/material"
import CapodoperaZilei from './components/Componente/CapodoperaZilei/CapodoperaZilei';
import Reviews from './components/Componente/Reviews/Reviews';

export default function Demo() {
    const history=useNavigate()
    return(
        <Box >
        
        {/* <Button variant='contained' onClick={()=>history("/test")}>Start Quiz</Button>
        <InfoSection/>
        <CapodoperaZilei/>
        <MesajeAcasa/>
        <div className='rand'>
          <div className="poza">
            <CardEchipa/>
          </div>
          <div className="poza">
          <CardEchipa/>
          </div>
          <div className="poza">
          <CardEchipa/>
          </div>
          
        </div> */}  
        
       
        <Stack direction="row" spacing={2} justifyContent="space-between">

          <HomePage/>
          <Reviews/>
         
        </Stack>
        
      </Box>
    )
};
