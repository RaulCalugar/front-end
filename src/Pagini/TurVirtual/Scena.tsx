import React,{useEffect} from "react";
import { Canvas,useThree } from '@react-three/fiber';
import { Physics, useBox, usePlane } from "@react-three/cannon";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
//@ts-ignore
import * as THREE from "three";
import "./style.css"
interface ICub{
    poz:THREE.Vector3
}

interface IPlan{
    culoare:string,
    poz:THREE.Vector3
}
const CameraController = () => {
  const { camera, gl } = useThree();
  useEffect(
    () => {
      const controls = new OrbitControls(camera, gl.domElement);

      controls.minDistance = 3;
      controls.maxDistance = 20;
      return () => {
        controls.dispose();
      };
    },
    [camera, gl]
  );
  return null;
};
function Cub({poz}:ICub){
  
    const textura=new THREE.TextureLoader().load("https://source.unsplash.com/random")
    return(
      <mesh position={poz}>
        <boxBufferGeometry  attach="geometry" args={[1,4,1]}/>
        <meshLambertMaterial attach="material"  map={textura}/>
  
      </mesh>
    )
  }

function Plan({culoare,poz}:IPlan){

    return (
      <>
      <mesh rotation={[-Math.PI / 2, 0, 0]}  position={poz}>
        <planeGeometry args={[1000, 1000]} />
        <meshLambertMaterial color={culoare} />
      </mesh>
      <gridHelper />
      <axesHelper />
      </>
  
      
    );
  }

  function Cub1(){
    return(
      <mesh>
    <boxGeometry args={[3, 1, 2]} />
    <meshStandardMaterial color="orange" />
  </mesh>
    )
  }

  export default function Scena(){

    return(
        <>
            <Canvas camera={{ position: [0, 0, 0], near: 0.1, far: 1000 }}>
              <CameraController/>
                <Physics gravity={[0, -10, 0]}>
                {/* <Plan culoare="yellow" poz={new THREE.Vector3( 0, -2, 0 )} />
                <Plan culoare="green" poz={new THREE.Vector3( 0, 0, -10 )}/> */}
                <Cub poz={new THREE.Vector3( 2,10,-5 )}/>
                <Cub poz={new THREE.Vector3( 0,6,-5 )}/>
                <Cub poz={new THREE.Vector3( -2,5,-5 )}/>
                {/* <Cub1 /> */}
                </Physics>
                <ambientLight intensity={0.3} />
                <pointLight intensity={0.8} position={[5, 0, 5]} />
            </Canvas>
        </>
    )
  }