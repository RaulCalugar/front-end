import React, { useEffect,useRef,useMemo } from "react";
import { Canvas, useFrame,useThree } from '@react-three/fiber'
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { PerspectiveCamera } from "three";
import {Physics,usePlane} from "@react-three/cannon"
import {Box} from "@mui/material"
//    import { Stars, Sky, /* Stats */ } from "@react-three/drei";

//@ts-ignore
import * as THREE from "three";

function Cub(){
  
  const textura=new THREE.TextureLoader().load("https://source.unsplash.com/random")
  return(
    <mesh position={[0,8,10]}>
      <boxBufferGeometry  attach="geometry" args={[5, 25, -1.2]}/>
      <meshLambertMaterial attach="material"  map={textura}/>

    </mesh>
  )
}



function Plan(){

  const [ref] = usePlane(() => ({
    rotation: [-Math.PI / 2, 0, 0],
  }));
  return (
    <>
    <mesh rotation={[-Math.PI / 2, 0, 0]} receiveShadow position={[0, -0.1, 22]}>
      <planeGeometry args={[80, 100]} />
      <meshLambertMaterial color="#1ea3d8" />
    </mesh>
    <gridHelper />
    <axesHelper />
    </>

    
  );
}

const CameraController = () => {
  const { camera, gl } = useThree();
  useEffect(
    () => {
      const controls = new OrbitControls(camera, gl.domElement);

      controls.minDistance = 3;
      controls.maxDistance = 20;
      return () => {
        controls.dispose();
      };
    },
    [camera, gl]
  );
  return null;
};

export default function TurVirtual() {
    

  return (
   
    <div style={{ width: "100%", height: "100vh" }}>
    <Canvas>
      {/* <perspectiveCamera fov={750} aspect={window.innerWidth/window.innerHeight}
      position={[10,-10,40]} near={10} far={10}> */}
      <CameraController/>
      <ambientLight intensity={0.5} />
      <spotLight position={[10, 15, 10]} angle={0.3} />
      <Physics>
        <Cub/>
        <Plan/>
      </Physics>
      {/* </perspectiveCamera> */}
    </Canvas>
    </div>

  );
}

const Ground = () => {
  let marbleAlphaMap, marbleMap, marbleNormalMap, grassMap;
  const size = 4.6;

  const [ref] = usePlane(() => ({ 
      rotation: [-Math.PI / 2, 0, 0],
      position: [0, 0.1, 22],
  }));

  marbleMap = useMemo(() => new THREE.TextureLoader().load("https://source.unsplash.com/random"), []);
  marbleMap.wrapS = THREE.MirroredRepeatWrapping;
  marbleMap.wrapT = THREE.MirroredRepeatWrapping;
  marbleMap.repeat.set(size, size);

  marbleAlphaMap = useMemo(() => new THREE.TextureLoader().load("https://source.unsplash.com/random"), []);
  marbleAlphaMap.wrapS = THREE.MirroredRepeatWrapping;
  marbleAlphaMap.wrapT = THREE.MirroredRepeatWrapping;
  marbleAlphaMap.repeat.set(size, size);

  marbleNormalMap = useMemo(() => new THREE.TextureLoader().load("https://source.unsplash.com/random"), []);
  marbleNormalMap.wrapS = THREE.MirroredRepeatWrapping;
  marbleNormalMap.wrapT = THREE.MirroredRepeatWrapping;
  marbleNormalMap.repeat.set(size, size);

  grassMap = useMemo(() => new THREE.TextureLoader().load("https://source.unsplash.com/random"), []);
  grassMap.wrapS = THREE.RepeatWrapping;
  grassMap.wrapT = THREE.RepeatWrapping;
  grassMap.repeat.set(70, 70);

  return (
      <>
          <mesh rotation={[-Math.PI / 2, 0, 0]} position={[0, -0.3, 22]} >
              <planeBufferGeometry attach="geometry" args={[1000, 1000]} />
              <meshLambertMaterial attach="material">
                  <primitive attach="map" object={grassMap} />
              </meshLambertMaterial>
          </mesh>
            
          <mesh rotation={[-Math.PI / 2, 0, 0]} position={[0, -0.1, 22]} >
                  <planeBufferGeometry attach="geometry" args={[70, 75]}  />
          </mesh>

          <mesh ref={ref} receiveShadow>
              <planeBufferGeometry attach="geometry" args={[70, 75]} />
              <meshPhysicalMaterial 
                  attach="material"
                  reflectivity={0}
                  clearcoat={1}
                  transparent
                  roughness={0.5}
                  metalness={0.3}
              >
                  <primitive attach="map" object={marbleMap} />
                  <primitive attach="alphaMap" object={marbleAlphaMap} />
                  <primitive attach="normalMap" object={marbleNormalMap} />
              </meshPhysicalMaterial>
          </mesh>
      </>
  );
}