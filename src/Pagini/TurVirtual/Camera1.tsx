import { Canvas,Camera, useThree,useFrame } from "@react-three/fiber";
import React, {useEffect, Suspense ,useRef} from "react";

import ReactDOM from "react-dom";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

function Box() {
  
  return (
    <mesh position={[1.2, 0, 0]}  >
      <boxBufferGeometry attach="geometry" args={[1, 2, 1]} />
      <meshStandardMaterial attach="material" color="orange" />
    </mesh>
  )
}

function Plan(){
  return(
    <mesh rotation={[-Math.PI/2,0,0]} position={[0, 2.5, -5]}>
      <planeGeometry />
      <meshLambertMaterial color="red"/>
    </mesh>
  )
}

export default function Camera1() {

  return (
    <div style={{ height: '100vh' }}>
      <Canvas gl={{ autoClear: false }} >
        <ambientLight />
        <Box  />
        <Plan />
      </Canvas>
    </div>
  )
}