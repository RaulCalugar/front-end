import React, { useState, useEffect } from "react";
import CardArtistLocal from "../../components/Componente/CardArtistLocal/CardArtistLocal";
import { Linkuri } from "../../Utils/Linkuri";
import { img, IArtistLocal, IOperaLocala, getImageFromByteArray } from "../../Utils/Utils";
import { Checkbox, ListItemText, MenuItem, OutlinedInput, Select,SelectChangeEvent ,Typography } from "@mui/material";

interface IJudet {
    auto: string,
    nume: string
}

export default function ArtistiLocali() {

    const [artisti, setArtisti] = useState<Array<IArtistLocal>>([])
    const [artistAfisati,setArtistiAfisati]=useState<Array<IArtistLocal>>([])
    const [poze, setPoza] = useState<string[]>([])
    const [judete, setJudete] = useState<string[]>([])
    const [judet,setJudet]=useState<string[]>([])

    useEffect(() => {
        const artistiLocali = async () => {
            const data = await fetch(Linkuri.backend + Linkuri.utilizator + Linkuri.artistiLocali)
            const json = await data.json()
            setArtisti(json)
            setArtistiAfisati(json)
        }
        let temp: string[] = []
        fetch(Linkuri.localitati + Linkuri.judete).then(function (body) {
            return body.json()
        }).then(function (data) {
            for (var i = 0; i < data.length; i++) {
                temp[i] = data[i].nume
            }
            setJudete(temp)

        })
        artistiLocali()


    }, [])

    useEffect(()=>{
        if(judet.length===0){
            setArtistiAfisati(artisti)
            return
        }
        const data:IArtistLocal[]=artisti.filter((item:IArtistLocal)=>{
            if(judet.includes(item.judet)){
                return item
            }
        
        })
        setArtistiAfisati(data)
    },[judet])


   
    return (

        <>
            <Typography>
                Selectare Artisti Locali Dintr-un Anumit Judet
            </Typography>
            <Select
                labelId="demo-multiple-checkbox-label"
                id="demo-multiple-checkbox"
                value={judet}
                fullWidth
                multiple
                onChange={(ev:any)=>setJudet(ev.target.value)}
                input={<OutlinedInput label="Tag" />}
                renderValue={(selected) => selected.join(', ')}
                MenuProps={MenuProps}
                sx={{width:'300px',marginBottom:'140px'}}
            >
                {judete.map((name) => (
                    <MenuItem key={name} value={name}>
                        {/* <Checkbox inputProps={{ 'aria-label': 'controlled' }} /> */}
                        <ListItemText primary={name} />
                    </MenuItem>
                ))}
            </Select>
            {
                artistAfisati.map((item) => (
                    <CardArtistLocal key={item.email} info={item} />
                ))
            }
        </>
    )
}

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250
        }
    }
};