import React,{useState,useEffect} from 'react'
import { Linkuri } from '../../Utils/Linkuri'
import { IUtilizator , getImageFromByteArray } from '../../Utils/Utils'
import { useCookies } from 'react-cookie'
import {Box,Card,CardMedia,CardContent,Typography} from "@mui/material"

export default function ProfilUtilizator(){


    const [utilizator,setUtilizator]=useState<IUtilizator>()
    const [cookies,setCookies]=useCookies(['jwt'])

    useEffect(()=>{
        const preluareUtilizator=async()=>{
            const data=await fetch(Linkuri.backend+Linkuri.utilizator+Linkuri.detaliiUtilizator,{
                headers:{
                    'Authorization': cookies.jwt
                }
            })
            const json=await data.json()
            setUtilizator(json)
        }

        preluareUtilizator()
    },[])

    return(
        <>
            <Box flex={4} p={{ xs: 0, md: 2 }}>
            <Card sx={{ margin: 5 }}>
                <div style={{display:"flex"}}>
                <CardMedia
                    component="img"
                    height="20%"
                    width="50% !important"
                    image={"data:image/png;base64," + utilizator?.pozaProfil}
                    alt={utilizator?.prenume+ " "+utilizator?.nume}
                    
                />
                <CardContent>
                    <Typography variant="body2" color="text.primary">
                        NUME : {utilizator?.nume}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Prenume: {utilizator?.prenume}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Email: {utilizator?.email}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Numar Telefon: {utilizator?.numarTelefon}
                    </Typography>
                  
                </CardContent>
                </div>
                
            </Card>
        </Box>
        </>
    )
}

