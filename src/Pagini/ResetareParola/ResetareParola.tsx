import React, { useState } from 'react'
import Swal from 'sweetalert2';

import { Linkuri } from '../../Utils/Linkuri';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { Container, Typography, Link, Grid, Box, Button, CssBaseline, TextField, Avatar, breadcrumbsClasses } from '@mui/material';
import { Raspunsuri, raspuns, eroareSistem, Icons, Mesaje,campObligatoriu } from '../../Utils/Utils';
const theme = createTheme();
export default function ResetareParola() {

    const [email, setEmail] = useState<string>("")
    const [parola, setParola] = useState<string>("")
    const [cod, setCod] = useState<string>("")
    const [trimis, setTrimis] = useState<boolean>(false)
    const [codTrimis, setCodTrimis] = useState<boolean>(false)


    const generareCod = async (event: React.FormEvent<HTMLFormElement>) => {

        event.preventDefault()
        console.log(email)
        const res = await fetch(Linkuri.backend + Linkuri.utilizator + Linkuri.generareCod, {
            body: email,
            method: "post"
        })

        const data = await res.json()
        switch (data) {
            case Raspunsuri.NO_EMAIL:
                raspuns("Eroare", Icons.eroare, "Nu exista un cont aferent acestui mail")
                break;
            case Raspunsuri.SUCCESS:
                raspuns("Mail-ul a fost trimis cu succes", Icons.succes, "Introduceti codul care a fost trimis la adresa de mail introdusa de dumneavoastra")
                setTrimis(true)
                break;
            default:
                eroareSistem();
                break;
        }


    }

    const verificareCod = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()

        const verificareDto = {
            email: email,
            cod: cod
        }
        console.log(verificareDto)
        const res = await fetch(Linkuri.backend + Linkuri.utilizator + Linkuri.verificareCod, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(verificareDto)
        })
        const data = await res.json()
        console.log("DATA:"+data)
        switch(data){
            case Raspunsuri.COD_GRESIT:
                raspuns("Eroare",Icons.eroare,"A-ti introdus un cod gresit");
                break;
            case Raspunsuri.EXPIRAT:
                raspuns("Eroare",Icons.eroare,"Termenul de valabilitate a codului a expirat")
                Swal.fire({
                    title:"Termenul de valabilitate a codului a expirat",
                    text:"Doriti sa vi se trimita un alt cod ?",
                    icon:Icons.question,
                    showCancelButton:true,
                    cancelButtonColor:'#a76384',
                    cancelButtonText:"NU",
                    showConfirmButton:true,
                    confirmButtonColor:'#b80391',
                    confirmButtonText:"DA"
                }).then((res)=>{
                    if(res.isConfirmed){
                        setTrimis(!trimis)
                    }
                })
                break;
            case Raspunsuri.SUCCESS:
                raspuns("SUCCESS",Icons.succes,"Codul introdus de dumneavoastra este valid");
                setCodTrimis(!codTrimis)
                break;
            default:
                eroareSistem();
                break;
        }
        console.log(data)
    }

    const schimbareParola = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        const schimbareParolaDto={
            "email":email,
            "parola":parola
        }
        const res = await fetch(Linkuri.backend + Linkuri.utilizator + Linkuri.schimbareParola, {
            body: JSON.stringify(schimbareParolaDto),
            method: "post",
            headers: {
                'Content-Type': 'application/json',
            }
        })

        const data=await res.json()
        switch(data){
            case Raspunsuri.NO_EMAIL:
                raspuns("EROARE",Icons.eroare,Mesaje.NO_EMAIL);
                break;
            case Raspunsuri.SUCCESS:
                raspuns("FELICITARI",Icons.succes,"Parola dumneavoastra a fost modificata cu succes");
                setTrimis(false)
        }
    }

    return (
        <>
            <h1 className="titlu">
                Pagina Resetare Parola
            </h1>

            <ThemeProvider theme={theme}>
                <Container component="main" maxWidth="xs">
                    <CssBaseline />
                    <Box
                        sx={{
                            marginTop: 8,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>

                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Resetare Parola
                        </Typography>
                        <Box component="form" noValidate sx={{ mt: 1 }}>
                            {
                                !trimis ? (
                                    <>
                                        <TextField margin='normal' required fullWidth id='email' label='EMAIL'
                                            name='email'
                                            value={email}
                                            onChange={(ev) => setEmail(ev.target.value)}
                                            error={email.length===0}
                                            helperText={campObligatoriu}
                                        />
                                        <Button
                                            type="submit"
                                            fullWidth
                                            onClick={(event: any) => { generareCod(event) }}
                                            variant="contained"
                                            sx={{ mt: 3, mb: 2 }}

                                        >
                                            Generare Cod</Button>
                                    </>

                                ) :
                                    
                                            !codTrimis ? 
                                                (<>
                                                    <TextField margin='normal' required fullWidth id='cod' label='COD'
                                                        name='cod'
                                                        value={cod}
                                                        onChange={(e) => setCod(e.target.value)}
                                                        error={cod.length===0}
                                                        helperText={campObligatoriu}
                                                    />
                                                    <Button
                                                        type="submit"
                                                        fullWidth
                                                        variant="contained"
                                                        onClick={(event: any) => { verificareCod(event) }}
                                                        sx={{ mt: 3, mb: 2 }}>
                                                        Verificare Cod</Button>
                                                </>)
                                             : (
                                                <>
                                                    <TextField margin='normal' required fullWidth id='parola' label='INTRODUCETI NOUA PAROLA' value={parola}
                                                    onChange={(ev)=>setParola(ev.target.value)}
                                                    error={parola.length===0}
                                                    helperText={campObligatoriu}
                                                    name='parola' />
                                                    <Button
                                                        type="submit"
                                                        fullWidth
                                                        variant="contained"
                                                        sx={{ mt: 3, mb: 2 }}
                                                        onClick={(event: any) => { schimbareParola(event) }}
                                                        >
                                                        Schimbare Parola</Button>
                                                </>
                                            )
                                        }
                                  

                        </Box>
                    </Box>
                </Container>
            </ThemeProvider>
        </>
    )
}