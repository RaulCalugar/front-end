import React from "react";
import CapodoperaZilei from "../../components/Componente/CapodoperaZilei/CapodoperaZilei";
import InfoSection from "../../components/Componente/InfoSection/InfoSection";
import MesajeAcasa from "../../components/Componente/MesajeAcasa/MesajeAcasa";
import {Box} from "@mui/material"
import { useNavigate } from "react-router-dom";
import PanouControl from "../../components/Componente/PanouControl/PanouControl";
import OpereAleatoare from "../../components/Componente/OpereAleatoare/OpereAleatoare";
export default function HomePage() {

    return(
        <Box flex={6}>
        <PanouControl/>
        <InfoSection/> 
        <CapodoperaZilei/>
        <MesajeAcasa/>
        
        {/* <OpereAleatoare /> */}
        </Box>
        
    )
};
