import React, { useState, useEffect,FC} from "react";
import { Raspunsuri, IArtist, IOpera,IDetaliiOpera } from "../../Utils/Utils";
import { useParams, useNavigate} from "react-router-dom";
import { Linkuri } from "../../Utils/Linkuri";
import { Box, CircularProgress, Typography, Grid } from "@mui/material"
import { Button, Card, CardMedia, CardContent, CardHeader, CardActions, Container, Divider } from "@mui/material"
import SimpleReactLightbox, { SRLWrapper } from 'simple-react-lightbox';
import CSS from 'csstype';
import { useTheme } from "@mui/material/styles";
import EditareOpera from "../../components/Componente/EditareOpera/EditareOpera";



interface IOpereSimilare {
    numeAutor: string,
    opera: IOpera
}

interface Autor{
    state:{autor:string}
}

 const Opera:FC=()=> {

    const { opera } = useParams()
    const [detalii, setDetalii] = useState<IDetaliiOpera>()
    const [opereArtist, setOpereArtist] = useState<Array<IOpera>>([])
    const [opereSimilare, setOpereSimilare] = useState<Array<IOpereSimilare>>([])
    const [autor, setAutor] = useState<string>("")
    const [load, setLoad] = useState<boolean>(true)
    const [link, setLink] = useState<string>("")
    const [id,setId]=useState<number>(0)
    let nume = ""



    const history = useNavigate()
    const detaliiOpera = async () => {
        setLoad(true)
        const data = await fetch(Linkuri.backend + Linkuri.artist + Linkuri.detaliiOpera + "/" + opera)
        const json = await data.json()
        nume = json.autor
        setAutor(json.autor)
        setId(json.id)
        setDetalii(json)

    }

    const getOpereSimilare = async () => {
        const url = Linkuri.backend + Linkuri.muzeu + Linkuri.opereSimilare
        const data = await fetch(url)
        const json = await data.json()
        setOpereSimilare(json)
        setLoad(false)
    }

    const picturiArtist = async () => {
        const url = Linkuri.backend + Linkuri.artist + Linkuri.picturiArtist + nume
        const data = await fetch(url)
        const json = await data.json()
        console.log(url)
        setOpereArtist(json.list)
        setLoad(false)
    }



    useEffect(() => {
        setLoad(true)
        // fetch(Linkuri.backend+Linkuri.opera+Linkuri.detaliiOpera+"/"+opera).then(res=>res.json()).then(res=>{

        //     (setDetalii(res));
        //     setAutor(res.autor)
        //     nume=res.autor
        // })  
        detaliiOpera()
        fetch(Linkuri.backend + Linkuri.muzeu + Linkuri.opereSimilare).then(res => res.json()).then(res => setOpereSimilare(res))
        const url = Linkuri.backend + Linkuri.artist + Linkuri.picturiArtist + nume
        setLoad(false)
        // fetch(url).then(res=>res.json()).then((res)=>{

        //     setOpereArtist(res.list)
        //     setLoad(false)
        // })
        // detaliiOpera()
        // getOpereSimilare()
        // picturiArtist()



    }, [opera])

    const theme = useTheme();
    return (

        <>  
            {load ? (<CircularProgress color="secondary" />) : (<>
                {/* Grid Card */}
                <Container maxWidth="xl">
                    <Grid container spacing={3} >
                        <Grid item xs={16} sm={16} md={16} lg={14} xl={13} sx={{ width: "100%" }}>
                            <Card sx={{ ...CARD_PROPERTY }}>
                                <Box sx={{ display: "flex" }}>
                                    <Box
                                        sx={{ display: "flex", alignItems: "center", pl: 1, pb: 1 }}
                                    >


                                        <CardMedia
                                            component="img"
                                            image={detalii?.urlPoza}
                                            height="650"
                                            width='250'
                                            alt={detalii?.nume}
                                        />
                                    </Box>
                                    <Box sx={{ display: "flex", flexDirection: "column" }}>
                                        <CardContent sx={{ flex: "1 0 auto" }}>
                                            <Typography
                                                component="div"
                                                variant="h5"
                                                sx={{ fontWeight: 500 }}
                                            >
                                               Nume: {detalii?.nume}
                                               
                                            </Typography>
                                            <Typography
                                                variant="subtitle1"
                                                color="text.secondary"
                                                component="div"
                                                onClick={()=>history("/artist/"+detalii?.autor)}
                                            >
                                               Autor: {detalii?.autor}
                                            </Typography>
                                            
                                            <Typography>An aparitie: {detalii?.anAparitie}</Typography>
                                        </CardContent>

                                    </Box>

                                </Box>
                            </Card>
                        </Grid>
                    </Grid>
                </Container>
                {/* <Card >
                <CardHeader>{detalii?.nume}</CardHeader>
                <Divider/>
                    <Box sx={{display:'flex',width:'300px'}}>
                        <CardMedia component='img' alt={detalii?.nume} image={detalii?.urlPoza} />
                        <CardContent>
                            <Typography>{detalii?.nume}</Typography>
                        </CardContent>
                    </Box>
            </Card> */}
                <Typography variant="h1">Lucrari {detalii?.autor}</Typography>
                {/* <SimpleReactLightbox>
                <SRLWrapper>
                    <Box >
                        {
                            opereArtist.map((opera)=>(
                                <Card key={opera.nume}>
                                    <CardMedia 
                                        component="img" alt={opera.nume} height='160' image={opera.urlPoza} />
                                    <CardActions>
                                        <Button>Detalii</Button>
                                    </CardActions>
                                </Card>
                            ))
                        }
                        
                    </Box>
                    </SRLWrapper>
           </SimpleReactLightbox> */}  
                <Typography variant="h1">Opere similare</Typography>
                <EditareOpera numeOpera={opera!} />
                {/* !!! OPERE SIMILARE */}
                {/* <Divider/>
                <SimpleReactLightbox>
                <SRLWrapper>
                <Box sx={{ ...imagini }}>
                    {
                        opereSimilare.map((opera) => (
                            <Card sx={{ width: '400px', height: '520px' }} key={opera.opera.urlPoza}>
                                <CardMedia
                                    component="img" alt={opera.opera.nume} height='450' image={opera.opera.urlPoza}
                                />
                                <CardActions sx={{justifyContent:'center'}}>
                                    <Button onClick={() => history("/opera/" + (opera.opera.nume+","+opera.numeAutor))}>Detalii</Button>
                                </CardActions>
                            </Card>
                        ))
                    }
                </Box>
                </SRLWrapper>
                </SimpleReactLightbox> */}

            </>

            )}
        </>
    )
}

export default Opera;
const CARD_PROPERTY: CSS.Properties = {
    borderRadius: '3',
    boxShadow: '0'
};

const imagine: CSS.Properties = {
    width: "100%",
    height: "100%",
    objectFit: "cover"
}

const imagini: CSS.Properties = {
    display: "grid",
    gap: "0.5rem",
    gridTemplateColumns: "repeat(auto-fill,minmax(450px,1fr))"
}