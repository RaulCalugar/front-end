import React, { useState, useEffect } from "react"
import PanouControl from "../../components/Componente/PanouControl/PanouControl"
import { Linkuri } from "../../Utils/Linkuri"

export default function Admin() {


    useEffect(() => {

     
        fetch(Linkuri.backend + Linkuri.artistLocal + "/poze")
            .then(response => response.text())
            .then(data => {
                
                setImg(data)

            })
        // obtinePicturi()

    }, [])
    const [img, setImg] = useState({})
    return (
        <>
            <PanouControl />
            <img
                src={`data:image/jpeg;base64,${img}`}
                alt=""
                />
        </>
    )
}