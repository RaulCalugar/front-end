import React,{useEffect,useState} from "react";
import {Box, Stack,CircularProgress } from "@mui/material"
import { useParams } from 'react-router-dom'
import InfoAutor from "../../components/Componente/InfoAutor/InfoAutor";
import ArtistiSimilari from "../../components/Componente/ArtistiSimilari/ArtistiSimilari";
export default function Artist(){

    const {numeArtist}=useParams()
    const [load,setLoad]=useState<boolean>(false)
    useEffect(()=>{

    },[numeArtist])

    return(
        
        <Box>
           {load ? (  <CircularProgress color="secondary" />):
            (<>
                <Stack direction="row"  justifyContent="space-between">
                <InfoAutor numeAutor={numeArtist!}  load={load} setLoad={setLoad}/>  
                <ArtistiSimilari/>
            </Stack>
            </>)
           }
            
        </Box>
    )
}
