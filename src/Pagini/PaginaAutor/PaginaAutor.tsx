import React,{useState,useEffect} from "react";
import { Linkuri } from "../../Utils/Linkuri";
import { eroareSistem, Raspunsuri ,IOpera,IArtist} from "../../Utils/Utils";
import {Box,CircularProgress,Typography,ImageList,ImageListItem, Stack} from "@mui/material"
import CardInfoAutor from "../../components/Componente/CardInfoAutor/CardInfoAutor";
import CSS from 'csstype';
import InfoAutor from "../../components/Componente/InfoAutor/InfoAutor";
import ArtistiSimilari from "../../components/Componente/ArtistiSimilari/ArtistiSimilari";
export default function PaginaAutor(){
    const [load,setLoad]=useState<boolean>(false)
    return(
        
        
        <Box>
           {load ? (  <CircularProgress color="secondary" />):
            (<>
                <Stack direction="row"  justifyContent="space-between">
                <InfoAutor numeAutor="Theodor Aman"  load={load} setLoad={setLoad}/>  
                <ArtistiSimilari/>
            </Stack>
            </>)
           }
            
        </Box>
    )
}