import React,{useState,useEffect} from "react";
import { litere,IArtist } from "../../Utils/Utils";
import {Box,Button,Grid} from "@mui/material"
import { Linkuri } from "../../Utils/Linkuri";
import CardAutor from "../../components/Componente/CardAutor/CardAutor";
import TravelExploreIcon from '@mui/icons-material/TravelExplore';
import { useNavigate } from "react-router-dom";
import AfisareArtisti from "../../components/Componente/AfisareArtisti/AfisareArtisti"
import BaraCautare from "../../components/Componente/BaraCautare/BaraCautare";

export default function Artisti(){

    const [total, setTotal]=useState<number>(0)
    const [limita, setLimita]=useState<number>(60)// Numar de artisti afisati pe pagina
    const [afisare, setAfisare]=useState<number>(limita)//Cati artisti afisez pe pagina la un moment dat
    const [artisti,setArtisti]=useState<Array<IArtist>>([])
    
    const history=useNavigate()
    useEffect(()=>{
        const totiArtistii=async()=>{
            const data=await fetch(Linkuri.backend+Linkuri.artist+Linkuri.totiArtistii);
            const json=await data.json()
            setTotal(json.length)
            setArtisti(json)
            
        }
        totiArtistii()
    },[])

   
    return(
        <Box>
            <Box >
            {
                litere.map(litera=><Button onClick={()=>history("/artisti/"+litera)} key={litera}>{litera}</Button>)
                
            }
            </Box>
            <BaraCautare artisti={artisti}/>
            <AfisareArtisti artisti={artisti} limita={limita} afisare={afisare} total={total}/>
          
        </Box>
    )
}