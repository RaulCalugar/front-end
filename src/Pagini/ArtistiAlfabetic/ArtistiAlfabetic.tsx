import React,{useEffect,useState} from "react";
import { useParams } from 'react-router-dom'
import { Linkuri } from "../../Utils/Linkuri";
import { IArtist } from "../../Utils/Utils";
import AfisareArtisti from "../../components/Componente/AfisareArtisti/AfisareArtisti";
import {Box} from "@mui/material"
export default function ArtistiAlfabetic(){

    const {litera}=useParams()

    const [total, setTotal]=useState<number>(0)
    const [limita, setLimita]=useState<number>(60)// Numar de artisti afisati pe pagina
    const [afisare, setAfisare]=useState<number>(limita)//Cati artisti afisez pe pagina la un moment dat
    const [artisti,setArtisti]=useState<Array<IArtist>>([])


    useEffect(()=>{
        
        const artisti=async()=>{
            const data=await fetch(Linkuri.backend+Linkuri.artist+Linkuri.totiArtistii+"/"+litera);
            const json=await data.json()
            setTotal(json.length)
            setArtisti(json)

        }
        artisti()

    },[])

    return(
        <Box>
            <AfisareArtisti artisti={artisti} limita={limita} afisare={afisare} total={total}/>
        </Box>

    )
}