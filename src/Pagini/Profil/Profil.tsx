import React,{useState,useEffect} from 'react'

import {Box,Card,CardContent,CardHeader,CardMedia,Typography} from "@mui/material";
import ReviewCard from '../../components/Componente/ReviewCard/ReviewCard';

export default function Profil(){

    return(

        <>
            <Box flex={4} p={{ xs: 0, md: 2 }}>
            <Card sx={{ margin: 5 }}>
                <CardHeader  
                    title="Capodopera Zilei"
                      
                />
                <div style={{display:"flex",height:"100%"}}>

                
                <CardMedia
                    component="img"
                    height="20%"
                    width="70% !important"
                    image="https://upload.wikimedia.org/wikipedia/commons/6/6e/Ncolae_Grigorescu_%281860%29.JPG"
                    // image="https://images.pexels.com/photos/4534200/pexels-photo-4534200.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
                    alt="Paella dish"
                    
                />
                <CardContent>
                    <Typography variant="body2" color="text.primary">
                        NUME CAPODOPERA: MONALISA
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        AUTOR: MONALISA
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Ani de viata autor: 1920-1999
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Info:
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                    Bun venit la Muzart, locul unde arta, spectacolul si inovatia fac echipa pentru a va oferi o experienta culturala de neuitat. Designul atractiv, organizarea 
atenta si multitudinea de lucrari artistice pe care le puteti studia si despre care puteti afla informatii interesante face din Muzart locul perfect pentru a petrece timp admirand arta si 
imbogatindu-va cultura.
                    </Typography>
                </CardContent>
                </div>
                
            </Card>
        </Box>

        <Box ml={10} sx={{
            justifyContent: "center",
            alignItems: "center" }}>
            <Box sx={{backgroundColor:"lightblue",width:"90%"}}>
                <Typography>Descriere</Typography>
                <Typography>
                    Emanuel este un tanar de 23 de ani, extrem de pasionat de istorie si de tot ceea ce inseamna arta. A fost 
                    student la Facultatea de Istorie din cadrul universitatii Babes Boliyai. Este de asemenea si doctorand in Istorie.
                    Este un excelent gdid, avand abilitati de comunicare foarte bune.
                    Emanuel este un tanar de 23 de ani, extrem de pasionat de istorie si de tot ceea ce inseamna arta. A fost 
                    student la Facultatea de Istorie din cadrul universitatii Babes Boliyai. Este de asemenea si doctorand in Istorie.
                    Este un excelent gdid, avand abilitati de comunicare foarte bune.
                    Emanuel este un tanar de 23 de ani, extrem de pasionat de istorie si de tot ceea ce inseamna arta. A fost 
                    student la Facultatea de Istorie din cadrul universitatii Babes Boliyai. Este de asemenea si doctorand in Istorie.
                    Este un excelent gdid, avand abilitati de comunicare foarte bune.
                    Emanuel este un tanar de 23 de ani, extrem de pasionat de istorie si de tot ceea ce inseamna arta. A fost 
                    student la Facultatea de Istorie din cadrul universitatii Babes Boliyai. Este de asemenea si doctorand in Istorie.
                    Este un excelent gdid, avand abilitati de comunicare foarte bune.
                    Emanuel este un tanar de 23 de ani, extrem de pasionat de istorie si de tot ceea ce inseamna arta. A fost 
                    student la Facultatea de Istorie din cadrul universitatii Babes Boliyai. Este de asemenea si doctorand in Istorie.
                    Este un excelent gdid, avand abilitati de comunicare foarte bune.
                </Typography>
            </Box>
        </Box>
        <Box>
            <ReviewCard/>
            <ReviewCard/>


        </Box>
        </>
    )
}

