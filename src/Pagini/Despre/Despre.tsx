import React, { useState, useEffect } from "react";
import EchipaNoastra from "../../components/Componente/EchipaNoastra/EchipaNoastra";
import { Linkuri } from "../../Utils/Linkuri";
import { Box, Typography, ImageList, ImageListItem } from "@mui/material"
import { eroareSistem, IOpera } from "../../Utils/Utils";
export default function Despre() {

  const [descriere, setDescriere] = useState<string>("")
  const [opere, setOpere] = useState<Array<IOpera>>([])

  useEffect(() => {
    fetch(Linkuri.backend + Linkuri.muzeu + Linkuri.descriere).
      then(function (body) {
        return body.text();
      }).then(function (data) {
        setDescriere(data)
      })//.catch(()=>eroareSistem())

      const obtinePicturi=async()=>{
        const data=await fetch(Linkuri.backend+Linkuri.muzeu+Linkuri.opere);
        const json=await data.json();
        setOpere(json.list)
        
      }

      obtinePicturi()

  }, [])
  return (

    <>
      <Box ml={10} sx={{
        justifyContent: "center",
        alignItems: "center"
      }}>
        <Box sx={{ backgroundColor: "lightblue", width: "90%" }}>
          <Typography>Descriere</Typography>
          <Typography>
            {descriere}
          </Typography>
        </Box>
      </Box>
      <EchipaNoastra />
      <ImageList sx={{ width: 500, height: 450 }} cols={3} rowHeight={164}>
        {opere.map((item) => (
          <ImageListItem key={item.urlPoza}>
            <img
              onMouseOver={()=>console.log(item.nume)}
              src={`${item.urlPoza}?w=164&h=164&fit=crop&auto=format`}
              srcSet={`${item.urlPoza}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
              alt={item.nume}
              loading="lazy"
            />
          </ImageListItem>
        ))}
      </ImageList>

    </>
  )
}

