import React,{useEffect,useState} from "react";
import { useParams } from "react-router-dom";

import { Linkuri } from "../../Utils/Linkuri";
import { getImageFromByteArray, IArtistLocal,IOperaLocala } from "../../Utils/Utils";
import CardInfoArtistLocal from "../../components/Componente/CardInfoArtistLocal/CardInfoArtistLocal";
import CardArtistLocal from "../../components/Componente/CardArtistLocal/CardArtistLocal";
export default function ArtistLocal(){

    const {nume}=useParams()
    const [artist,setArtist]=useState<IArtistLocal>()
    const [im,setIm]=useState<string>("")

    useEffect(()=>{

        const artistLocal=async()=>{
            const data=await fetch(Linkuri.backend+Linkuri.utilizator+Linkuri.artistLocal+nume)
            const json=await data.json()
            setArtist(json)
            
        }
        artistLocal()
        
        
    },[])
    return(
        <>
            <p>{artist?.email}</p>
            <CardInfoArtistLocal {...artist!}/>
            {
                artist?.opere.map((opera:IOperaLocala)=>(
                    <img src={getImageFromByteArray(opera.poza)} key={opera.nume} />
                ))
            }
        </>
    )
}